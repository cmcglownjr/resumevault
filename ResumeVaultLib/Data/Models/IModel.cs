namespace ResumeVaultLib.Data.Models
{
    public interface IModel
    {
        public int ID { get; set; }
        public string Name { get; }
    }
}