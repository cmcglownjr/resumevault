namespace ResumeVaultLib.Data.Models
{
    public class TagExperienceBridgeModel : IModel
    {
        public int ID { get; set; }
        public string Name { get; }
        public int ExperienceID { get; set; }
        public int TagID { get; set; }
    }
}