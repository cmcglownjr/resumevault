using System;

namespace ResumeVaultLib.Data.Models
{
    public class MiscModel : IModel
    {
        public int ID { get; set; }
        public int PersonID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime Date { get; set; }
        public int Category { get; set; }
    }
}