namespace ResumeVaultLib.Data.Models
{
    public class PersonModel : IModel
    {
        public int ID { get; set; }
        public string Name => FullName;
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string ContactNumber { get; set; }
        public string ContactEmail { get; set; }
        public string LinkedIn { get; set; }
        public string FullName => $"{FirstName} {LastName}";
    }
}