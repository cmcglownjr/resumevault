namespace ResumeVaultLib.Data.Models
{
    public class SkillCategoryModel : IModel
    {
        public int ID { get; set; }
        public string Name { get; set; }
    }
}