namespace ResumeVaultLib.Data.Models
{
    public class TagSkillBridgeModel : IModel
    {
        private IModel _modelImplementation;
        public int ID { get; set; }
        public string Name { get; }
        public int SkillID { get; set; }
        public int TagID { get; set; }
    }
}