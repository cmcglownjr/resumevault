namespace ResumeVaultLib.Data.Models
{
    public class TagModel : IModel
    {
        public int ID { get; set; }
        public string Name { get; set; }
    }
}