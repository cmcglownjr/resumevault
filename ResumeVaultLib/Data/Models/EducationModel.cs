using System;

namespace ResumeVaultLib.Data.Models
{
    public class EducationModel : IModel
    {
        public int ID { get; set; }

        public string Name => SchoolName;
        public string SchoolName { get; set; }
        public string Major { get; set; }
        public string Minor { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public decimal GPA { get; set; }
        public bool Attending { get; set; }
        public int PersonID { get; set; }

        public string Education
        {
            get
            {
                var minor = Minor;
                if (Minor is not null)
                {
                    minor = "/" + Minor;
                }
                if (Attending)
                {
                    return $"Currently attending {SchoolName},starting on {StartDate.ToString("yyyy-MM-dd")}. Major/Minor: {Major}{minor}. GPA: {GPA}";
                }
        
                return $"Attended {SchoolName} between {StartDate.ToString("yyyy-MM-dd")} and {EndDate.ToString("yyyy-MM-dd")}. Major/Minor: {Major}{minor}. GPA: {GPA}";
            }
        }
    }
}