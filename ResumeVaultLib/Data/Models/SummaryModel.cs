namespace ResumeVaultLib.Data.Models
{
    public class SummaryModel : IModel
    {
        public int ID { get; set; }
        public int PersonID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}