namespace ResumeVaultLib.Data.Models
{
    public class AddressModel : IModel
    {
        public int ID { get; set; }
        public int PersonID { get; set; }
        public string Name { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public int StateProvence { get; set; }
        public string StateProvenceString { get; set; }
        public string Zip { get; set; }
    }
}