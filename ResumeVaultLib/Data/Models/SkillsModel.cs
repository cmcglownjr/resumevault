using System;

namespace ResumeVaultLib.Data.Models
{
    public class SkillsModel : IModel
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public DateTime StartDate { get; set; }
        public int PersonID { get; set; }
        public int Category { get; set; }
    }
}