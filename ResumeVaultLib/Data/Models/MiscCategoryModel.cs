namespace ResumeVaultLib.Data.Models
{
    public class MiscCategoryModel : IModel
    {
        public int ID { get; set; }
        public string Name { get; set; }
    }
}