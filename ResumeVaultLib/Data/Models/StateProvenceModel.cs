namespace ResumeVaultLib.Data.Models
{
    public class StateProvenceModel : IModel
    {
        public int ID { get; set; }
        public string Name => $"{StateProvence}, {Country}";
        public string StateProvence { get; set; }
        public string Country { get; set; }
    }
}