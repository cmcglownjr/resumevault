namespace ResumeVaultLib.Data.Models
{
    public class ExperienceCategoryModel : IModel
    {
        public int ID { get; set; }
        public string Name { get; set; }
    }
}