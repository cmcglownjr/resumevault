using System;

namespace ResumeVaultLib.Data.Models
{
    public class ExperienceModel : IModel
    {
        private DateTime _startDate;
        private DateTime _endDate;
        public int ID { get; set; }
        public string Name => Organization;
        public int PersonID { get; set; }
        public string Organization { get; set; }
        public string Title { get; set; }
        public string Details { get; set; }
        public DateTime StartDate 
        { 
            get { return _startDate;}
            set { _startDate = value; }
        }

        public DateTime EndDate
        {
            get { return _endDate;}
            set { _endDate = value; }
        }
        public decimal StartSalary { get; set; }
        public decimal EndSalary { get; set; }
        public bool Current { get; set; }
        public int Category { get; set; }

        public string Experience
        {
            get
            {
                if (Current)
                {
                    return $"Member of {Organization}. Title: {Title}. Start date: {StartDate.ToString("yyyy-MM-dd")}.";
                }

                return @$"Was a member of {Organization} and held the title of {Title} between {StartDate.ToString("yyyy-MM-dd")} and {EndDate.ToString("yyyy-MM-dd")}.";
            }
        }
    }
}