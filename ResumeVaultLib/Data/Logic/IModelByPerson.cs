using System.Collections.Generic;

namespace ResumeVaultLib.Data.Logic
{
    public interface IModelByPerson : IModelProcessor
    {
        public List<T> LoadDataListByPerson<T>(T model);
    }
}