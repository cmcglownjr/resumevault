using System.Collections.Generic;
using System.Text;
using ResumeVaultLib.Data.Utilities;

namespace ResumeVaultLib.Data.Logic
{
    public class EducationProcessor : IModelByPerson
    {
        private readonly IDatabaseAccess _databaseAccess;

        public EducationProcessor(IDatabaseAccess databaseAccess)
        {
            _databaseAccess = databaseAccess;
        }
        public void Insert<T>(T model)
        {
            string sql = @"insert into Education 
                        (SchoolName, Major, Minor, StartDate, EndDate, GPA, Attending, PersonID) VALUES 
                        (@SchoolName, @Major, @Minor, @StartDate, @EndDate, @GPA, @Attending, @PersonID)";
            _databaseAccess.InsertQuery(model, sql);
        }

        public void Update<T>(T model)
        {
            string sql = @"update Education set SchoolName=@SchoolName, Major=@Major, Minor=@Minor, 
                     StartDate=@StartDate, EndDate=@EndDate, GPA=@GPA, Attending=@Attending, PersonID=@PersonID where 
                     ID=@ID";
            _databaseAccess.UpdateQuery(model, sql);
        }

        public void Delete<T>(T model)
        {
            string sql = "delete from Education where ID=@ID";
            _databaseAccess.DeleteQuery(model, sql);
        }

        public T LoadDataSingle<T>(T model)
        {
            string sql = "select * from Education where ID=@ID";
            return _databaseAccess.SelectQuerySingle(model, sql);
        }

        public List<T> LoadAllDataList<T>(T model)
        {
            string sql = "select * from Education";
            return _databaseAccess.SelectQueryList(model, sql);
        }

        public List<T> LoadDataListByPerson<T>(T model)
        {
            string sql = "select * from Education where PersonID=@PersonID";
            return _databaseAccess.SelectQueryList(model, sql);
        }
        public List<T> LoadSelectDataList<T>(T model, List<int> idList)
        {
            StringBuilder builder = new StringBuilder();
            foreach (var item in idList)
            {
                if (idList.IndexOf(item) == idList.Count - 1)
                {
                    builder.Append(item);
                }
                else
                {
                    builder.Append(item).Append(", ");
                }
            }

            var ids = builder.ToString();
            string sql = $"select * from Education where ID in ({ids});";
            return _databaseAccess.SelectQueryList(model, sql);
        }
    }
}