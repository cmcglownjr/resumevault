using System.Collections.Generic;
using ResumeVaultLib.Data.Utilities;

namespace ResumeVaultLib.Data.Logic
{
    public class TagSkillBridgeProcessor : IModelProcessor
    {
        private IDatabaseAccess _databaseAccess;

        public TagSkillBridgeProcessor(IDatabaseAccess databaseAccess)
        {
            _databaseAccess = databaseAccess;
        }
        public void Insert<T>(T model)
        {
            string sql = "insert into TagSkillBridge (SkillID, TagID) values (@SkillID, @TagID)";
            _databaseAccess.InsertQuery(model, sql);
        }

        public void Update<T>(T model)
        {
            string sql = "update TagSkillBridge set SkillID=@SkillID, TagID=@TagID where ID=@ID";
            _databaseAccess.UpdateQuery(model, sql);
        }

        public void Delete<T>(T model)
        {
            string sql = "delete from TagSkillBridge where ID=@ID";
            _databaseAccess.DeleteQuery(model, sql);
        }

        public T LoadDataSingle<T>(T model)
        {
            string sql = "select * from TagSkillBridge where ID=@ID";
            return _databaseAccess.SelectQuerySingle(model, sql);
        }

        public List<T> LoadAllDataList<T>(T model)
        {
            string sql = "select * from TagSkillBridge";
            return _databaseAccess.SelectQueryList(model, sql);
        }
    }
}