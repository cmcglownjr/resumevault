using System.Collections.Generic;
using ResumeVaultLib.Data.Utilities;

namespace ResumeVaultLib.Data.Logic
{
    public class SkillCategoryProcessor : IModelProcessor
    {
        private IDatabaseAccess _databaseAccess;

        public SkillCategoryProcessor(IDatabaseAccess databaseAccess)
        {
            _databaseAccess = databaseAccess;
        }
        public void Insert<T>(T model)
        {
            string sql = "insert into SkillCategory (Name) values (@Name)";
            _databaseAccess.InsertQuery(model, sql);
        }

        public void Update<T>(T model)
        {
            string sql = "update SkillCategory set Name=@Name where ID=@ID";
            _databaseAccess.UpdateQuery(model, sql);
        }

        public void Delete<T>(T model)
        {
            string sql = "delete from SkillCategory where ID=@ID";
            _databaseAccess.DeleteQuery(model, sql);
        }

        public T LoadDataSingle<T>(T model)
        {
            string sql = "select * from SkillCategory where ID=@ID";
            return _databaseAccess.SelectQuerySingle(model, sql);
        }

        public List<T> LoadAllDataList<T>(T model)
        {
            string sql = "select * from SkillCategory";
            return _databaseAccess.SelectQueryList(model, sql);
        }
    }
}