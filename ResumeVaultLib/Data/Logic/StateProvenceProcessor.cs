using System.Collections.Generic;
using ResumeVaultLib.Data.Utilities;

namespace ResumeVaultLib.Data.Logic
{
    public class StateProvenceProcessor : IModelProcessor
    {
        private IDatabaseAccess _databaseAccess;

        public StateProvenceProcessor(IDatabaseAccess databaseAccess)
        {
            _databaseAccess = databaseAccess;
        }
        public void Insert<T>(T model)
        {
            string sql = "insert into StateProvence (StateProvence, Country) values (@StateProvence, @Country)";
            _databaseAccess.InsertQuery(model, sql);
        }

        public void Update<T>(T model)
        {
            string sql = "update StateProvence set StateProvence=@StateProvence, Country=@Country where ID+@ID";
            _databaseAccess.UpdateQuery(model, sql);
        }

        public void Delete<T>(T model)
        {
            string sql = "delete from StateProvence where ID=@ID";
            _databaseAccess. DeleteQuery(model, sql);
        }

        public T LoadDataSingle<T>(T model)
        {
            string sql = "select * from StateProvence where ID=@ID";
            return _databaseAccess.SelectQuerySingle(model, sql);
        }

        public List<T> LoadAllDataList<T>(T model)
        {
            string sql = "select * from StateProvence";
            return _databaseAccess.SelectQueryList(model, sql);
        }
    }
}