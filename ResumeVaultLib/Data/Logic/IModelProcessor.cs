using System.Collections.Generic;

namespace ResumeVaultLib.Data.Logic
{
    public interface IModelProcessor
    {
        void Insert<T>(T model);
        void Update<T>(T model);
        void Delete<T>(T model);
        T LoadDataSingle<T>(T model);
        List<T> LoadAllDataList<T>(T model);
    }
}