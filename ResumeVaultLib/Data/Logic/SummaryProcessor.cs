using System.Collections.Generic;
using ResumeVaultLib.Data.Utilities;

namespace ResumeVaultLib.Data.Logic
{
    public class SummaryProcessor : IModelByPerson
    {
        private IDatabaseAccess _databaseAccess;

        public SummaryProcessor(IDatabaseAccess databaseAccess)
        {
            _databaseAccess = databaseAccess;
        }
        public void Insert<T>(T model)
        {
            string sql = "insert into Summary (PersonID, Name, Description) values (@PersonID, @Name, @Description)";
            _databaseAccess.InsertQuery(model, sql);
        }

        public void Update<T>(T model)
        {
            string sql = "update Summary set PersonID=@PersonID, Name=@Name, Description=@Description where ID=@ID";
            _databaseAccess.UpdateQuery(model, sql);
        }

        public void Delete<T>(T model)
        {
            string sql = "delete from Summary where ID=@ID";
            _databaseAccess.DeleteQuery(model, sql);
        }

        public T LoadDataSingle<T>(T model)
        {
            string sql = "select * from Summary where ID=@ID";
            return _databaseAccess.SelectQuerySingle(model, sql);
        }

        public List<T> LoadAllDataList<T>(T model)
        {
            string sql = "select * from Summary";
            return _databaseAccess.SelectQueryList(model, sql);
        }

        public List<T> LoadDataListByPerson<T>(T model)
        {
            string sql = @"select * from Summary where PersonID=@PersonID";
            return _databaseAccess.SelectQueryList(model, sql);
        }
    }
}