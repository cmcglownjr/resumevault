using System.Collections.Generic;
using ResumeVaultLib.Data.Utilities;

namespace ResumeVaultLib.Data.Logic
{
    public class PersonProcessor : IModelProcessor
    {
        private IDatabaseAccess _databaseAccess;

        public PersonProcessor(IDatabaseAccess databaseAccess)
        {
            _databaseAccess = databaseAccess;
        }

        public void Insert<T>(T model)
        {
            string sql = @"insert into Person (FirstName, LastName, ContactNumber, ContactEmail, LinkedIn) Values 
            (@FirstName, @LastName, @ContactNumber, @ContactEmail, @LinkedIn)";
            _databaseAccess.InsertQuery(model, sql);
        }

        public void Update<T>(T model)
        {
            string sql = @"update Person set FirstName=@FirstName, LastName=@LastName, ContactNumber=@ContactNumber, 
            ContactEmail=@ContactEmail, LinkedIn=@LinkedIn where ID=@ID";
            _databaseAccess.UpdateQuery(model, sql);
        }

        public void Delete<T>(T model)
        {
            string sql = "delete from Person where ID=@ID";
            _databaseAccess.DeleteQuery(model, sql);
        }

        public T LoadDataSingle<T>(T model)
        {
            string sql = "select * from Person where ID=@ID";
            return _databaseAccess.SelectQuerySingle(model, sql);
        }

        public List<T> LoadAllDataList<T>(T model)
        {
            string sql = "select * from Person";
            return _databaseAccess.SelectQueryList(model, sql);
        }
    }
}