using System.Collections.Generic;
using ResumeVaultLib.Data.Utilities;

namespace ResumeVaultLib.Data.Logic
{
    public class TagExperienceBridgeProcessor : IModelProcessor
    {
        private IDatabaseAccess _databaseAccess;

        public TagExperienceBridgeProcessor(IDatabaseAccess databaseAccess)
        {
            _databaseAccess = databaseAccess;
        }
        public void Insert<T>(T model)
        {
            string sql = "insert into TagExperienceBridge (ExperienceID, TagID) values (@ExperienceID, @TagID)";
            _databaseAccess.InsertQuery(model, sql);
        }

        public void Update<T>(T model)
        {
            string sql = "update TagExperienceBridge set ExperienceID=@ExperienceID, TagID=@TagID where ID=@ID";
            _databaseAccess.UpdateQuery(model, sql);
        }

        public void Delete<T>(T model)
        {
            string sql = "delete from TagExperienceBridge where ID=@ID";
            _databaseAccess.DeleteQuery(model, sql);
        }

        public T LoadDataSingle<T>(T model)
        {
            string sql = "select * from TagExperienceBridge where ID=@ID";
            return _databaseAccess.SelectQuerySingle(model, sql);
        }

        public List<T> LoadAllDataList<T>(T model)
        {
            string sql = "select * from TagExperienceBridge";
            return _databaseAccess.SelectQueryList(model, sql);
        }
    }
}