using System.Collections.Generic;
using ResumeVaultLib.Data.Utilities;

namespace ResumeVaultLib.Data.Logic
{
    public class AddressProcessor : IModelByPerson
    {
        private IDatabaseAccess _databaseAccess;

        public AddressProcessor(IDatabaseAccess databaseAccess)
        {
            _databaseAccess = databaseAccess;
        }
        public void Insert<T>(T model)
        {
            string sql =  @"insert into Address (PersonID, Address1, Address2, City, StateProvence, Zip, Name) 
                            VALUES (@PersonID, @Address1, @Address2, @City, @StateProvence, @Zip, @Name)";
            _databaseAccess.InsertQuery(model, sql);
        }

        public void Update<T>(T model)
        {
            string sql = @"update Address set PersonID=@PersonID, Address1=@Address1, Address2=@Address2, 
                            City=@City, StateProvence=@StateProvence, Zip=@Zip, Name=@Name where ID=@ID";
            _databaseAccess.UpdateQuery(model, sql);
        }

        public void Delete<T>(T model)
        {
            string sql = "delete from Address where ID=@ID";
            _databaseAccess.DeleteQuery(model, sql);
        }

        public T LoadDataSingle<T>(T model)
        {
            string sql = "select * from Address where ID=@ID";
            return _databaseAccess.SelectQuerySingle(model, sql);
        }

        public List<T> LoadAllDataList<T>(T model)
        {
            string sql = "select * from Address";
            return _databaseAccess.SelectQueryList(model, sql);
        }

        public List<T> LoadDataListByPerson<T>(T model)
        {
            string sql = @"select * from Address where PersonID=@PersonID";
            return _databaseAccess.SelectQueryList(model, sql);
        }
    }
}