using System.Collections.Generic;
using System.Text;
using ResumeVaultLib.Data.Utilities;

namespace ResumeVaultLib.Data.Logic
{
    public class ExperienceProcessor : IModelByPerson
    {
        private readonly IDatabaseAccess _databaseAccess;

        public ExperienceProcessor(IDatabaseAccess databaseAccess)
        {
            _databaseAccess = databaseAccess;
        }
        public void Insert<T>(T model)
        {
            string sql = @"insert into Experience (PersonID, Organization, Title, Details, StartDate, EndDate, Current, 
                        StartSalary, EndSalary, Category) VALUES (@PersonID, @Organization, @Title, @Details, 
                                                                  @StartDate, @EndDate, @Current, @StartSalary, 
                                                                  @EndSalary, @Category)";
            _databaseAccess.InsertQuery(model, sql);
        }

        public void Update<T>(T model)
        {
            string sql = @"update Experience set PersonID=@PersonID, Organization=@Organization, Title=@Title, 
                      Details=@Details, StartDate=@StartDate, EndDate=@EndDate, Current=@Current, 
                      StartSalary=@StartSalary, EndSalary=@EndSalary, Category=@Category where ID=@ID";
            _databaseAccess.UpdateQuery(model, sql);
        }

        public void Delete<T>(T model)
        {
            string sql = "delete from Experience where ID=@ID";
            _databaseAccess.DeleteQuery(model, sql);
        }

        public T LoadDataSingle<T>(T model)
        {
            string sql = "select * from Experience where ID=@ID";
            return _databaseAccess.SelectQuerySingle(model, sql);
        }

        public List<T> LoadAllDataList<T>(T model)
        {
            string sql = "select * from Experience";
            return _databaseAccess.SelectQueryList(model, sql);
        }

        public List<T> LoadDataListByPerson<T>(T model)
        {
            string sql = "select * from Experience where PersonID=@PersonID";
            return _databaseAccess.SelectQueryList(model, sql);
        }
        public List<T> LoadSelectDataList<T>(T model, List<int> idList)
        {
            StringBuilder builder = new StringBuilder();
            foreach (var item in idList)
            {
                if (idList.IndexOf(item) == idList.Count - 1)
                {
                    builder.Append(item);
                }
                else
                {
                    builder.Append(item).Append(", ");
                }
            }

            var ids = builder.ToString();
            string sql = $"select * from Experience where ID in ({ids}) ORDER BY StartDate DESC";
            return _databaseAccess.SelectQueryList(model, sql);
        }
    }
}