using System.Collections.Generic;
using ResumeVaultLib.Data.Utilities;

namespace ResumeVaultLib.Data.Logic
{
    public class MiscCategoryProcessor : IModelProcessor
    {
        private IDatabaseAccess _databaseAccess;

        public MiscCategoryProcessor(IDatabaseAccess databaseAccess)
        {
            _databaseAccess = databaseAccess;
        }
        public void Insert<T>(T model)
        {
            string sql = @"insert into Misc (PersonID, Name, Description, Date, Category) values 
                        (@PersonID, @Name, @Description, @Date, @Category)";
            _databaseAccess.InsertQuery(model, sql);
        }

        public void Update<T>(T model)
        {
            string sql = @"update Misc set PersonID=@PersonID, Name=@Name, Description=@Description, Date=@Date, 
                        Category=@Category where ID=@ID";
            _databaseAccess.UpdateQuery(model, sql);
        }

        public void Delete<T>(T model)
        {
            string sql = "delete from MiscCategory where ID=@ID";
            _databaseAccess.DeleteQuery(model, sql);
        }

        public T LoadDataSingle<T>(T model)
        {
            string sql = "select * from MiscCategory where ID=@ID";
            return _databaseAccess.SelectQuerySingle(model, sql);
        }

        public List<T> LoadAllDataList<T>(T model)
        {
            string sql = "select * from MiscCategory";
            return _databaseAccess.SelectQueryList(model, sql);
        }
    }
}