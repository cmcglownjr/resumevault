using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.IO;
using System.Linq;
using System.Reflection;
using Dapper;
using Serilog;

namespace ResumeVaultLib.Data.Utilities
{
    public class SqliteDataAccess : IDatabaseAccess
    {
        private FileInfo _db;
        public FileInfo DB => _db;

        public SqliteDataAccess(FileInfo db = null)
        {
            _db = db;
        }
        public FileInfo CreateDatabase(string filename = null, string path = null)
        {
            string homeFolder = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile);
            if (filename is null || filename == "")
            {
                filename = "ResumeVault.db";
            }

            if (path is null || path == "")
            {
                path = homeFolder;
            }
            if (!filename.Contains(".db"))
            {
                filename += ".db";
            }
            string newDb = Path.Combine(path, filename);
            Assembly asm = Assembly.GetExecutingAssembly();
            StreamReader reader =
                new StreamReader(asm.GetManifestResourceStream("ResumeVaultLib.Data.Utilities.ResumeVault.sql"));
            SQLiteConnection.CreateFile(newDb);
            Log.Information($"New SQLite database created: {newDb}");
            using (SQLiteConnection connection = new SQLiteConnection($"data source={newDb}"))
            {
                using (SQLiteCommand command = new SQLiteCommand(connection))
                {
                    connection.Open();
                    command.CommandText = reader.ReadToEnd();
                    command.ExecuteNonQuery();
                }
            }
            _db = new FileInfo(newDb);
            return _db;
        }

        public void LoadDatabase(FileInfo db)
        {
            _db = db;
        }

        public void InsertQuery<T>(T model, string sql)
        {
            using SQLiteConnection conn = new SQLiteConnection($"data source={_db}");
            conn.Open();
            conn.Execute("PRAGMA foreign_keys=ON");
            conn.Execute(sql, model);
            Log.Information("Executing insert query:");
            Log.Information(sql);
        }

        public void UpdateQuery<T>(T model, string sql)
        {
            using SQLiteConnection conn = new SQLiteConnection($"data source={_db}");
            conn.Open();
            conn.Execute("PRAGMA foreign_keys=ON");
            conn.Execute(sql, model);
            Log.Information("Executing update query:");
            Log.Information(sql);
        }

        public void DeleteQuery<T>(T model, string sql)
        {
            using SQLiteConnection conn = new SQLiteConnection($"data source={_db}");
            conn.Open();
            conn.Execute("PRAGMA foreign_keys=ON");
            conn.Execute(sql, model);
            Log.Information("Executing delete query:");
            Log.Information(sql);
        }

        public T SelectQuerySingle<T>(T model, string sql)
        {
            using SQLiteConnection conn = new SQLiteConnection($"data source={_db}");
            conn.Open();
            conn.Execute("PRAGMA foreign_keys=ON");
            Log.Information("Executing select query:");
            Log.Information(sql);
            return conn.QueryFirst<T>(sql);
        }

        public List<T> SelectQueryList<T>(T model, string sql)
        {
            using SQLiteConnection conn = new SQLiteConnection($"data source={_db}");
            conn.Open();
            conn.Execute("PRAGMA foreign_keys=ON");
            var query = conn.Query<T>(sql, model);
            Log.Information("Executing select query:");
            Log.Information(sql, model);
            return query.ToList();
        }
    }
}