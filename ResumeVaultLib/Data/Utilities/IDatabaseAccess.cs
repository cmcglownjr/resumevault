using System.Collections.Generic;
using System.IO;

namespace ResumeVaultLib.Data.Utilities
{
    public interface IDatabaseAccess
    {
        FileInfo CreateDatabase(string filename, string path);
        void LoadDatabase(FileInfo db);
        void InsertQuery<T>(T model, string sql);
        void UpdateQuery<T>(T model, string sql);
        void DeleteQuery<T>(T model, string sql);
        T SelectQuerySingle<T>(T model, string sql);
        List<T> SelectQueryList<T>(T model, string sql);
    }
}