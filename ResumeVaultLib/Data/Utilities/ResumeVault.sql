-- Default database creation script.

-- Remove tables if they exist
DROP TABLE IF EXISTS Address;
DROP TABLE IF EXISTS Education;
DROP TABLE IF EXISTS Experience;
DROP TABLE IF EXISTS ExperienceCategory;
DROP TABLE IF EXISTS Misc;
DROP TABLE IF EXISTS MiscCategory;
DROP TABLE IF EXISTS Person;
DROP TABLE IF EXISTS SkillCategory;
DROP TABLE IF EXISTS Skills;
DROP TABLE IF EXISTS StateProvence;
DROP TABLE IF EXISTS Summary;
DROP TABLE IF EXISTS Tag;
DROP TABLE IF EXISTS lnktblTagExperience;
DROP TABLE IF EXISTS lnktblTagSkills;

--Create new tables
CREATE TABLE "Person" (
    "ID"	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
    "FirstName"	TEXT NOT NULL,
    "LastName"	TEXT NOT NULL,
    "ContactNumber" TEXT,
    "ContactEmail"  TEXT,
    "LinkedIn"  TEXT
);
CREATE TABLE "StateProvence" (
    "ID"	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
    "StateProvence"	TEXT NOT NULL,
    "Country"	TEXT NOT NULL
);
CREATE TABLE "Address" (
    "ID"	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
    "PersonID"	INTEGER NOT NULL,
    "Name"  TEXT NOT NULL,
    "Address1"	TEXT NOT NULL,
    "Address2"	TEXT,
    "City"	TEXT NOT NULL,
    "StateProvence"	INTEGER NOT NULL,
    "Zip"	TEXT NOT NULL,
    FOREIGN KEY("StateProvence") REFERENCES "StateProvence"("ID"),
    FOREIGN KEY("PersonID") REFERENCES "Person"("ID") ON DELETE CASCADE
);
CREATE TABLE "Education" (
    "ID" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
    "SchoolName"	TEXT,
    "Major"	TEXT,
    "Minor"	TEXT,
    "StartDate"	TEXT,
    "EndDate"	TEXT,
    "GPA"	NUMERIC,
    "Attending"	INTEGER,
    "PersonID"	INTEGER NOT NULL,
    FOREIGN KEY("PersonID") REFERENCES "Person"("ID") ON DELETE CASCADE
);
CREATE TABLE "ExperienceCategory" (
    "ID"	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
    "Name"	TEXT NOT NULL
);
CREATE TABLE "Experience" (
    "ID"	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
    "PersonID"	INTEGER NOT NULL,
    "Organization"	TEXT,
    "Title"	TEXT,
    "Details"	TEXT NOT NULL,
    "StartDate"	TEXT NOT NULL,
    "EndDate"	TEXT,
    "Current"	INTEGER NOT NULL,
    "StartSalary"   REAL,
    "EndSalary" REAL,
    "Category"	INTEGER NOT NULL,
    FOREIGN KEY("Category") REFERENCES "ExperienceCategory"("ID") ON DELETE CASCADE,
    FOREIGN KEY("PersonID") REFERENCES "Person"("ID") ON DELETE CASCADE
);
CREATE TABLE "MiscCategory" (
    "ID"	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
    "Name"	TEXT NOT NULL
);
CREATE TABLE "Misc" (
    "ID"	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
    "PersonID"	INTEGER NOT NULL,
    "Name"	TEXT NOT NULL,
    "Description"	TEXT NOT NULL,
    "Date"	TEXT,
    "Category"	INTEGER NOT NULL,
    FOREIGN KEY("Category") REFERENCES "MiscCategory"("ID") ON DELETE CASCADE,
    FOREIGN KEY("PersonID") REFERENCES "Person"("ID") ON DELETE CASCADE
);
CREATE TABLE "SkillCategory" (
    "ID"	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
    "Name"	TEXT NOT NULL
);
CREATE TABLE "Skills" (
    "ID"	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
    "Name"	TEXT NOT NULL,
    "StartDate"	TEXT,
    "PersonID"	INTEGER NOT NULL,
    "Category"	INTEGER NOT NULL,
    FOREIGN KEY("Category") REFERENCES "SkillCategory"("ID") ON DELETE CASCADE,
    FOREIGN KEY("PersonID") REFERENCES "Person"("ID") ON DELETE CASCADE
);
CREATE TABLE "Summary" (
    "ID"	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
    "PersonID"	INTEGER NOT NULL,
    "Name"  TEXT NOT NULL,
    "Description"	TEXT NOT NULL,
    FOREIGN KEY("PersonID") REFERENCES "Person"("ID") ON DELETE CASCADE
);
CREATE TABLE "Tag" (
    "ID"	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
    "Name"	TEXT NOT NULL
);
CREATE TABLE "TagExperienceBridge" (
    "ID"	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
    "ExperienceID"	INTEGER NOT NULL,
    "TagID"	INTEGER NOT NULL,
    FOREIGN KEY("TagID") REFERENCES "Tag"("ID") ON DELETE CASCADE,
    FOREIGN KEY("ExperienceID") REFERENCES "Experience"("ID") ON DELETE CASCADE
);
CREATE TABLE "TagSkillBridge" (
    "ID"	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
    "SkillID"	INTEGER NOT NULL,
    "TagID"	INTEGER NOT NULL,
    FOREIGN KEY("TagID") REFERENCES "Tag"("ID") ON DELETE CASCADE,
    FOREIGN KEY("SkillID") REFERENCES "Skills"("ID") ON DELETE CASCADE
);

-- Fill with initial data
INSERT INTO "StateProvence" (StateProvence, Country) VALUES ('AL', 'USA'), ('AK', 'USA'), ('AZ', 'USA'), ('AR', 'USA'),
                                                            ('CA', 'USA'), ('CO', 'USA'), ('CT', 'USA'), ('DE', 'USA'),
                                                            ('FL', 'USA'), ('GA', 'USA'), ('HI', 'USA'), ('ID', 'USA'),
                                                            ('IL', 'USA'), ('IN', 'USA'), ('IA', 'USA'), ('KS', 'USA'),
                                                            ('KY', 'USA'), ('LA', 'USA'), ('ME', 'USA'), ('MD', 'USA'),
                                                            ('MA', 'USA'), ('MI', 'USA'), ('MN', 'USA'), ('MS', 'USA'), 
                                                            ('MO', 'USA'), ('MT', 'USA'), ('NE', 'USA'), ('NV', 'USA'), 
                                                            ('NH', 'USA'), ('NJ', 'USA'), ('NM', 'USA'), ('NY', 'USA'), 
                                                            ('NC', 'USA'), ('ND', 'USA'), ('OH', 'USA'), ('OK', 'USA'), 
                                                            ('OR', 'USA'), ('PA', 'USA'), ('RI', 'USA'), ('SC', 'USA'), 
                                                            ('SD', 'USA'), ('TN', 'USA'), ('TX', 'USA'), ('UT', 'USA'),
                                                            ('VA', 'USA'), ('VT', 'USA'), ('WA', 'USA'), ('WV', 'USA'), 
                                                            ('WI', 'USA'), ('WY', 'USA'), ('AS', 'USA'), ('DC', 'USA'),
                                                            ('FM', 'USA'), ('GU', 'USA'), ('MH', 'USA'), ('MP', 'USA'), 
                                                            ('PW', 'USA'), ('PR', 'USA'), ('VI', 'USA');
INSERT INTO ExperienceCategory (Name) VALUES ('Professional'), ('Volunteer');
INSERT INTO MiscCategory (Name) VALUES ('Certifications'), ('Publications'), ('Awards');
INSERT INTO SkillCategory (Name) VALUES ('Technical'), ('Hard'), ('Soft'), ('Leadership'), ('Management'), ('Marketing'),
                                        ('Administrative');
