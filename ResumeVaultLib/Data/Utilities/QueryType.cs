namespace ResumeVaultLib.Data.Utilities
{
    public enum QueryType
    {
        Insert,
        Update,
        Delete
    }
}