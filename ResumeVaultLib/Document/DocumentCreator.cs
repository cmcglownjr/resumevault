using System;
using System.Collections.Generic;
using System.IO;
using Serilog;
using Syncfusion.DocIO;
using Syncfusion.DocIO.DLS;
using ResumeVaultLib.Data.Models;

namespace ResumeVaultLib.Document
{
    public class DocumentCreator
    {
        private readonly FileInfo _filename;
        private readonly WordDocument _document;
        private readonly string _font = "Arial";
        private readonly IWSection _bodySection;
        private readonly PersonModel _person;
        public DocumentCreator(FileInfo filename, PersonModel person)
        {
            WordDocument document = new WordDocument();
            _document = document;
            _filename = filename;
            _person = person;
            _bodySection = _document.AddSection();
        }

        public void AddPerson()
        {
            IWParagraph paragraph = _bodySection.AddParagraph();
            paragraph.ParagraphFormat.HorizontalAlignment = HorizontalAlignment.Center;
            IWTextRange textRange = paragraph.AppendText(_person.FullName);
            textRange.CharacterFormat.FontName = _font;
            textRange.CharacterFormat.FontSize = 18;
            textRange.CharacterFormat.Bold = true;
        }

        public void AddContact(AddressModel address)
        {
            IWTable table = _bodySection.AddTable();
            IWTextRange textRange;
            table.ResetCells(1, 2);
            table.TableFormat.Borders.BorderType = BorderStyle.None;
            table.Description = "Contact Info";
            textRange = table[0, 0].AddParagraph().AppendText(address.Address1);
            textRange.CharacterFormat.FontName = _font;
            if (!(address.Address2 == "" || address.Address2 is null))
            {
                textRange = table[0, 0].AddParagraph().AppendText(address.Address2);
                textRange.CharacterFormat.FontName = _font;
            }
            textRange = table[0, 0].AddParagraph()
                .AppendText($"{address.City}, {address.StateProvenceString} {address.Zip}");
            textRange.CharacterFormat.FontName = _font;
            textRange = table[0, 1].AddParagraph().AppendText("Email: ");
            textRange.CharacterFormat.FontName = _font;
            textRange = table[0, 1].LastParagraph.AppendHyperlink($"mailto:{_person.ContactEmail}", 
                $"{_person.ContactEmail}", HyperlinkType.EMailLink);
            textRange.CharacterFormat.FontName = _font;
            textRange = table[0, 1].AddParagraph().AppendText($"Number: {_person.ContactNumber}");
            textRange.CharacterFormat.FontName = _font;
            textRange = table[0, 1].AddParagraph().AppendHyperlink(_person.LinkedIn, _person.LinkedIn, HyperlinkType.WebLink);
            textRange.CharacterFormat.FontName = _font;
        }

        public void AddEducation(Dictionary<int, IModel> education)
        {
            IWTable table = _bodySection.AddTable();
            table.Title = "Education";
            IWTextRange textRange;
            WTableRow row = table.AddRow();
            WTableCell cell = row.AddCell();
            cell.AddParagraph().ParagraphFormat.HorizontalAlignment = HorizontalAlignment.Center;
            textRange = cell.LastParagraph.AppendText("Education");
            cell.CellFormat.Borders.Bottom.LineWidth = 1f;
            cell.CellFormat.Borders.Top.LineWidth = 1f;
            textRange.CharacterFormat.FontName = _font;
            textRange.CharacterFormat.Bold = true;
            float width1 = 0.75f * table.Width;
            float width2 = 0.25f * table.Width;
            foreach (var educationEntry in education)
            {
                EducationModel edu = (EducationModel) educationEntry.Value;
                row = table.AddRow(true, false);
                cell = row.AddCell();
                cell.CellFormat.Borders.BorderType = BorderStyle.None;
                textRange = cell.AddParagraph().AppendText(edu.SchoolName);
                textRange.CharacterFormat.FontName = _font;
                textRange.CharacterFormat.Bold = true;
                textRange = cell.AddParagraph().AppendText(edu.Major);
                textRange.CharacterFormat.FontName = _font;
                textRange.CharacterFormat.Bold = true;
                if (!(edu.Minor == "" || edu.Minor is null))
                {
                    textRange = cell.AddParagraph().AppendText($"Minor {edu.Minor}");
                    textRange.CharacterFormat.FontName = _font;
                    textRange.CharacterFormat.Bold = false;
                }

                if (edu.GPA != 0)
                {
                    textRange = cell.AddParagraph().AppendText($"GPA: {edu.GPA}");
                    textRange.CharacterFormat.FontName = _font;
                    textRange.CharacterFormat.Bold = false;
                }
                cell = row.AddCell();
                cell.CellFormat.Borders.BorderType = BorderStyle.None;
                textRange = cell.AddParagraph()
                    .AppendText($"{edu.StartDate.ToString("Y")} to {edu.EndDate.ToString("Y")}");
                textRange.CharacterFormat.FontName = _font;
                textRange.CharacterFormat.Bold = false;
                row.Cells[0].Width = width1;
                row.Cells[1].Width = width2;

            }
        }

        public void AddExperience(Dictionary<int, IModel> experience, string experienceType)
        {
            IWTable table = _bodySection.AddTable();
            table.Title = $"{experienceType} Experience";
            IWTextRange textRange;
            WTableRow row = table.AddRow();
            WTableCell cell = row.AddCell();
            cell.AddParagraph().ParagraphFormat.HorizontalAlignment = HorizontalAlignment.Center;
            textRange = cell.LastParagraph.AppendText($"{experienceType} Experience");
            cell.CellFormat.Borders.Bottom.LineWidth = 1f;
            cell.CellFormat.Borders.Top.LineWidth = 1f;
            textRange.CharacterFormat.FontName = _font;
            textRange.CharacterFormat.Bold = true;
            float width1 = 0.80f * table.Width;
            float width2 = 0.20f * table.Width;
            foreach (var expEntry in experience)
            {
                ExperienceModel exp = (ExperienceModel) expEntry.Value;
                row = table.AddRow(true, false);
                cell = row.AddCell();
                cell.CellFormat.Borders.BorderType = BorderStyle.None;
                textRange = cell.AddParagraph().AppendText(exp.Organization);
                textRange.CharacterFormat.FontName = _font;
                textRange.CharacterFormat.Bold = true;
                textRange = cell.AddParagraph().AppendText(exp.Title);
                textRange.CharacterFormat.FontName = _font;
                textRange.CharacterFormat.Bold = false;
                textRange.CharacterFormat.Italic = true;
                cell = row.AddCell();
                textRange = cell.AddParagraph()
                    .AppendText($"{exp.StartDate.ToString("Y")} to {exp.EndDate.ToString("Y")}");
                textRange.CharacterFormat.FontName = _font;
                textRange.CharacterFormat.Bold = false;
                row.Cells[0].Width = width1;
                row.Cells[1].Width = width2;
                row = table.AddRow(true, false);
                cell = row.AddCell();
                cell.Width = table.Width;
                textRange = cell.AddParagraph().AppendText(exp.Details);
                textRange.CharacterFormat.FontName = _font;
            }
        }

        public void AddSkills(Dictionary<int, IModel> skills, string skillsType)
        {
            int column = 1;
            IWTable table = _bodySection.AddTable();
            table.Title = $"{skillsType} Skills";
            IWTextRange textRange;
            WTableRow row = table.AddRow();
            WTableCell cell = row.AddCell();
            cell.AddParagraph().ParagraphFormat.HorizontalAlignment = HorizontalAlignment.Center;
            textRange = cell.LastParagraph.AppendText($"{skillsType} Skills");
            cell.CellFormat.Borders.Bottom.LineWidth = 1f;
            cell.CellFormat.Borders.Top.LineWidth = 1f;
            textRange.CharacterFormat.FontName = _font;
            textRange.CharacterFormat.Bold = true;
            foreach (var skillEntry in skills)
            {
                SkillsModel skill = (SkillsModel) skillEntry.Value;
                switch (column)
                {
                    case 1:
                        row = table.AddRow(true, false);
                        cell = row.AddCell();
                        cell.CellFormat.Borders.BorderType = BorderStyle.None;
                        textRange = cell.AddParagraph().AppendText(skill.Name);
                        cell.LastParagraph.ListFormat.ApplyDefBulletStyle();
                        column += 1;
                        break;
                    case 2:
                    case 3:
                        cell = row.AddCell();
                        cell.CellFormat.Borders.BorderType = BorderStyle.None;
                        textRange = cell.AddParagraph().AppendText(skill.Name);
                        cell.LastParagraph.ListFormat.ApplyDefBulletStyle();
                        column += 1;
                        break;
                }
                textRange.CharacterFormat.FontName = _font;
                textRange.CharacterFormat.Bold = false;
                if (column == 4)
                {
                    column = 1;
                }
            }
            
        }

        public void AddSummary(SummaryModel summary)
        {
            IWTable table = _bodySection.AddTable();
            table.Title = "Summary";
            IWTextRange textRange;
            WTableRow row = table.AddRow();
            WTableCell cell = row.AddCell();
            cell.AddParagraph().ParagraphFormat.HorizontalAlignment = HorizontalAlignment.Center;
            textRange = cell.LastParagraph.AppendText("Summary");
            cell.CellFormat.Borders.Bottom.LineWidth = 1f;
            cell.CellFormat.Borders.Top.LineWidth = 1f;
            textRange.CharacterFormat.FontName = _font;
            textRange.CharacterFormat.Bold = true;
            row = table.AddRow(true, false);
            cell = row.AddCell();
            textRange = cell.AddParagraph().AppendText(summary.Description);
            textRange.CharacterFormat.FontName = _font;
            textRange.CharacterFormat.Bold = false;
        }

        public void AddMisc(Dictionary<int, IModel> misc, string miscType)
        {
            IWTable table = _bodySection.AddTable();
            table.Title = miscType;
            IWTextRange textRange;
            WTableRow row = table.AddRow();
            WTableCell cell = row.AddCell();
            cell.AddParagraph().ParagraphFormat.HorizontalAlignment = HorizontalAlignment.Center;
            textRange = cell.LastParagraph.AppendText(miscType);
            cell.CellFormat.Borders.Bottom.LineWidth = 1f;
            cell.CellFormat.Borders.Top.LineWidth = 1f;
            textRange.CharacterFormat.FontName = _font;
            textRange.CharacterFormat.Bold = true;
            foreach (var miscItemEntry in misc)
            {
                MiscModel miscItem = (MiscModel) miscItemEntry.Value;
                row = table.AddRow(true, false);
                cell = row.AddCell();
                cell.CellFormat.Borders.BorderType = BorderStyle.None;
                textRange = cell.AddParagraph().AppendText(miscItem.Name);
                textRange.CharacterFormat.FontName = _font;
                textRange.CharacterFormat.Bold = true;
                row = table.AddRow(true, false);
                cell = row.AddCell();
                textRange = cell.AddParagraph().AppendText(miscItem.Description);
                textRange.CharacterFormat.FontName = _font;
                textRange.CharacterFormat.Bold = false;
            }
        }

        public void SaveDocument()
        {
            using (FileStream outFile = new FileStream(_filename.FullName, FileMode.Create))
            {
                _document.Save(outFile, FormatType.Docx);
                _document.Close();
            }
        }
    }
}