using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using ResumeVaultLib.Data.Models;
using ResumeVaultLib.Document;
using Splat;

namespace ResumeVaultAvalonia.Models
{
    public class VaultPerson
    {
        private readonly VaultData _vault;
        private PersonModel _person = new();
        private SummaryModel _summary = new();
        private AddressModel _address = new();
        private Dictionary<string, Dictionary<int, IModel>> _educations = new();
        private Dictionary<string, Dictionary<int, IModel>> _experiences = new();
        private Dictionary<string, Dictionary<int, IModel>> _skills = new();
        private Dictionary<string, Dictionary<int, IModel>> _misc = new();
        private Dictionary<int, string> _experienceTypes = new();
        private Dictionary<int, string> _skillTypes = new();
        private Dictionary<int, string> _miscTypes = new();

        private Dictionary<Type, int> typeDict = new()
        {
            {typeof(SummaryModel), 1},
            {typeof(AddressModel), 2},
            {typeof(EducationModel), 3},
            {typeof(ExperienceModel), 4},
            {typeof(SkillsModel), 5},
            {typeof(MiscModel), 6}
        };

        public VaultPerson()
        {
            _vault = (VaultData) Locator.Current.GetService(typeof(VaultData));
        }
        public PersonModel Person
        {
            get => _person;
            set => _person = value;
        }

        public AddressModel Address
        {
            get => _address;
            set => _address = value;
        }

        public SummaryModel Summary
        {
            get => _summary;
            set => _summary = value;
        }
        public Dictionary<string, Dictionary<int, IModel>> Educations => _educations;
        public Dictionary<string, Dictionary<int, IModel>> Experiences => _experiences;
        public Dictionary<string, Dictionary<int, IModel>> Skills => _skills;
        public Dictionary<string, Dictionary<int, IModel>> Misc => _misc;

        public void ClearDict(IModel model)
        {
            switch (typeDict[model.GetType()])
            {
                case 3:
                    _educations.Clear();
                    break;
                case 4:
                    _experiences.Clear();
                    break;
                case 5:
                    _skills.Clear();
                    break;
                case 6:
                    _misc.Clear();
                    break;
            }
        }

        public void SetModelDictionary(IModel model, bool addModel)
        {
            switch (typeDict[model.GetType()])
            {
                case 3:
                    // Education
                    EducationModel edu = (EducationModel) model;
                    SetDictionary(edu, _educations, "");
                    break;
                case 4:
                    // Experience
                    ExperienceModel exp = (ExperienceModel) model;
                    SetDictionary(exp, _experiences, _experienceTypes[exp.Category]);
                    break;
                case 5:
                    // Skills
                    SkillsModel skills = (SkillsModel) model;
                    SetDictionary(skills, _skills, _skillTypes[skills.Category]);
                    break;
                case 6:
                    // Misc
                    MiscModel misc = (MiscModel) model;
                    SetDictionary(misc, _misc, _miscTypes[misc.Category]);
                    break;
            }

            void SetDictionary(IModel innerModel, Dictionary<string, Dictionary<int, IModel>> dict, string type)
            {
                if (dict.ContainsKey(type))
                {
                    if (addModel)
                    {
                        dict[type].Add(innerModel.ID, innerModel);
                        return;
                    }
                    dict[type].Remove(innerModel.ID);
                    return;
                }

                if (addModel)
                {
                    dict.Add(type, new Dictionary<int, IModel> {{innerModel.ID, innerModel}});
                    return;
                }

                throw new DataException(
                    "You are attempting to remove an item that isn't present in the current dictionary.");
            }
            
        }

        public void GetCategories()
        {
            _experienceTypes.Clear();
            _skillTypes.Clear();
            _miscTypes.Clear();
            ExperienceCategoryModel exp = new();
            SkillCategoryModel skills = new();
            MiscCategoryModel misc = new();
            List<ExperienceCategoryModel> expList = _vault.ExperienceCategoryProcessor.LoadAllDataList(exp);
            foreach (ExperienceCategoryModel item in expList)
            {
                _experienceTypes.Add(item.ID, item.Name);
            }
            List<SkillCategoryModel> skillsList = _vault.SkillCategoryProcessor.LoadAllDataList(skills);
            foreach (SkillCategoryModel item in skillsList)
            {
                _skillTypes.Add(item.ID, item.Name);
            }
            List<MiscCategoryModel> miscList = _vault.MiscCategoryProcessor.LoadAllDataList(misc);
            foreach (MiscCategoryModel item in miscList)
            {
                _miscTypes.Add(item.ID, item.Name);
            }
        }

        public void GenerateDocument(FileInfo filename)
        {
            DocumentCreator document = new(filename, _person);
            document.AddPerson();
            if (_address.Name != null)
            {
                document.AddContact(_address);
            }
            if (_summary.Name != null)
            {
                document.AddSummary(_summary);
            }
            if (_educations.Count > 0)
            {
                foreach (var item in _educations)
                {
                    document.AddEducation(item.Value);
                }
            }
            if (_experiences.Count > 0)
            {
                foreach (var item in _experiences)
                {
                    document.AddExperience(item.Value, item.Key);
                }
            }
            if (_misc.Count > 0)
            {
                foreach (var item in _misc)
                {
                    document.AddMisc(item.Value, item.Key);
                }
            }
            if (_skills.Count > 0)
            {
                foreach (var item in _skills)
                {
                    document.AddSkills(item.Value, item.Key);
                }
            }
            document.SaveDocument();
        }
    }
}