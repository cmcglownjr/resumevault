namespace ResumeVaultAvalonia.Models
{
    public enum ResumeView
    {
        Person,
        Summary,
        Address,
        Education,
        Experience,
        Skills,
        Misc,
        Tag
    }
}