using System;
using Avalonia;
using Avalonia.Controls;
using Avalonia.Platform;
using MessageBox.Avalonia;
using MessageBox.Avalonia.DTO;
using MessageBox.Avalonia.Enums;

namespace ResumeVaultAvalonia.Models
{
    public class MessageBox
    {
        private static WindowIcon _windowIcon = null!;
        public MessageBox()
        {
            var assets = AvaloniaLocator.Current.GetService<IAssetLoader>();
            _windowIcon = new(assets.Open(new Uri("avares://ResumeVaultAvalonia/Assets/Documents-02-256.ico")));
        }
        public static void ResumeMessageBoxDialog(string title, string message, Icon icon)
        {
            var msBoxStandardWindow = MessageBoxManager
                .GetMessageBoxStandardWindow(new MessageBoxStandardParams{
                    ButtonDefinitions = ButtonEnum.Ok,
                    ContentTitle = title,
                    ContentMessage = message,
                    Icon = icon,
                    WindowIcon = _windowIcon
                });
            msBoxStandardWindow.Show();
        }
    }
}