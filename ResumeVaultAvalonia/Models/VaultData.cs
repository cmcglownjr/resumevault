using System;
using System.Collections.Generic;
using JetBrains.Annotations;
using ResumeVaultLib.Data.Models;
using ResumeVaultLib.Data.Utilities;
using ResumeVaultLib.Data.Logic;

namespace ResumeVaultAvalonia.Models
{
    public class VaultData
    {
        private IDatabaseAccess _databaseAccess;
        // Main Processors
        public PersonProcessor PersonProcessor;
        public SummaryProcessor SummaryProcessor;
        public AddressProcessor AddressProcessor;
        public EducationProcessor EducationProcessor;
        public ExperienceProcessor ExperienceProcessor;
        public SkillsProcessor SkillsProcessor;
        public MiscProcessor MiscProcessor;
        public TagProcessor TagProcessor;
        // Sub Processors
        public ExperienceCategoryProcessor ExperienceCategoryProcessor;
        public MiscCategoryProcessor MiscCategoryProcessor;
        public SkillCategoryProcessor SkillCategoryProcessor;
        public StateProvenceProcessor StateProvenceProcessor;
        // Fields
        [NotNull]
        public IDatabaseAccess DatabaseAccess
        {
            get => _databaseAccess;
            set => _databaseAccess = value ?? throw new ArgumentNullException(nameof(value));
        }

        public VaultData()
        {
            _databaseAccess = new SqliteDataAccess();
            PersonProcessor = new PersonProcessor(_databaseAccess);
            SummaryProcessor = new SummaryProcessor(_databaseAccess);
            AddressProcessor = new AddressProcessor(_databaseAccess);
            EducationProcessor = new EducationProcessor(_databaseAccess);
            ExperienceProcessor = new ExperienceProcessor(_databaseAccess);
            SkillsProcessor = new SkillsProcessor(_databaseAccess);
            MiscProcessor = new MiscProcessor(_databaseAccess);
            TagProcessor = new TagProcessor(_databaseAccess);
            ExperienceCategoryProcessor = new ExperienceCategoryProcessor(_databaseAccess);
            MiscCategoryProcessor = new MiscCategoryProcessor(_databaseAccess);
            SkillCategoryProcessor = new SkillCategoryProcessor(_databaseAccess);
            StateProvenceProcessor = new StateProvenceProcessor(_databaseAccess);
        }

        public List<PersonModel> GeneratePeople()
        {
            return PersonProcessor.LoadAllDataList(new PersonModel());
        }
        
    }
}