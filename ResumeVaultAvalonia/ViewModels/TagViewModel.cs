using System.Collections.Generic;
using Avalonia;
using Avalonia.Controls.ApplicationLifetimes;
using Avalonia.Controls.Selection;
using System.Reactive;
using ReactiveUI;
using ResumeVaultAvalonia.Models;
using ResumeVaultAvalonia.Views;
using ResumeVaultLib.Data.Models;
using ResumeVaultLib.Data.Utilities;
using Splat;

namespace ResumeVaultAvalonia.ViewModels
{
    public class TagViewModel : ViewModelBase
    {
        private List<TagModel> _tagList = new();
        private readonly VaultData _vault;
        private TagModel _selectedItem = null!;
        private string _tagName = null!, _tagNumber = null!;
        public List<TagModel> TagList
        {
            get => _tagList;
            set => this.RaiseAndSetIfChanged(ref _tagList, value);
        }

        public TagModel SelectedItem
        {
            get => _selectedItem;
            set => this.RaiseAndSetIfChanged(ref _selectedItem, value);
        }
        public SelectionModel<IModel> Selection { get; }
        public string TagName
        {
            get => _tagName;
            set => this.RaiseAndSetIfChanged(ref _tagName, value);
        }
        public string TagNumber
        {
            get => _tagNumber;
            set => this.RaiseAndSetIfChanged(ref _tagNumber, value);
        }
        public ReactiveCommand<Unit, Unit> NewBtnPressed { get; }
        public ReactiveCommand<Unit, Unit> EditBtnPressed { get; }
        public ReactiveCommand<Unit, Unit> DeleteBtnPressed { get; }

        public TagViewModel()
        {
            _vault = (VaultData) Locator.Current.GetService(typeof(VaultData));
            NewBtnPressed = ReactiveCommand.Create(OnNewPressed);
            EditBtnPressed = ReactiveCommand.Create(OnEditPressed);
            DeleteBtnPressed = ReactiveCommand.Create(OnDeletePressed);
            Selection = new SelectionModel<IModel>();
            Selection.SelectionChanged += SelectionChanged!;
        }
        private async void OnNewPressed()
        {
            var queryModel = new QueryViewModel {View = ResumeView.Tag, QueryType = QueryType.Insert};
            queryModel.QueryBuilder();
            var queryView = new QueryView {DataContext = queryModel};
            if (Application.Current.ApplicationLifetime is IClassicDesktopStyleApplicationLifetime desktop)
            {
                await queryView.ShowDialog(desktop.MainWindow);
                Populate();
            }
        }

        private async void OnEditPressed()
        {
            var queryModel = new QueryViewModel {View = ResumeView.Tag, QueryType = QueryType.Update};
            queryModel.QueryBuilder(_selectedItem);
            var queryView = new QueryView {DataContext = queryModel};
            if (Application.Current.ApplicationLifetime is IClassicDesktopStyleApplicationLifetime desktop)
            {
                await queryView.ShowDialog(desktop.MainWindow);
                Populate();
            }
        }

        private async void OnDeletePressed()
        {
            var queryModel = new QueryViewModel {View = ResumeView.Tag, QueryType = QueryType.Delete};
            queryModel.QueryBuilder(_selectedItem);
            var queryView = new QueryView {DataContext = queryModel};
            if (Application.Current.ApplicationLifetime is IClassicDesktopStyleApplicationLifetime desktop)
            {
                await queryView.ShowDialog(desktop.MainWindow);
                Populate();
            }
        }
        void SelectionChanged(object sender, SelectionModelSelectionChangedEventArgs e)
        {
            TagName = TagList[Selection.SelectedIndex].Name;
            TagNumber = TagList.Count.ToString();
        }

        public void Populate()
        {
            if (TagList.Count > 0)
            {
                TagList.Clear();
            }
            TagName = "";
            TagNumber = "";
            TagModel model = new();
            TagList = _vault.TagProcessor.LoadAllDataList(model);
        }
    }
}