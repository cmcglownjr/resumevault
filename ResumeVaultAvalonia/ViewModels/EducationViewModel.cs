using System.Collections.Generic;
using Avalonia;
using Avalonia.Controls.ApplicationLifetimes;
using Avalonia.Controls.Selection;
using System.Reactive;
using ReactiveUI;
using ResumeVaultAvalonia.Models;
using ResumeVaultAvalonia.Views;
using ResumeVaultLib.Data.Models;
using ResumeVaultLib.Data.Utilities;
using Splat;

namespace ResumeVaultAvalonia.ViewModels
{
    public class EducationViewModel : ViewModelBase
    {
        private List<EducationModel> _educationList = new();
        private VaultData _vault;
        private VaultPerson _vaultPerson;
        private EducationModel _selectedItem = null!;
        private string _schoolName = null!, _major = null!, _minor = null!, _startDate = null!, _endDate = null!, _gpa = null!, _attending = null!;

        public List<EducationModel> EducationList
        {
            get => _educationList;
            set => this.RaiseAndSetIfChanged(ref _educationList, value);
        }
        public SelectionModel<IModel> Selection { get; }

        public EducationModel SelectedItem
        {
            get => _selectedItem;
            set => this.RaiseAndSetIfChanged(ref _selectedItem, value);
        }

        public string SchoolName
        {
            get => _schoolName;
            set => this.RaiseAndSetIfChanged(ref _schoolName, value);
        }
        public string Major
        {
            get => _major;
            set => this.RaiseAndSetIfChanged(ref _major, value);
        }
        public string Minor
        {
            get => _minor;
            set => this.RaiseAndSetIfChanged(ref _minor, value);
        }
        public string StartDate
        {
            get => _startDate;
            set => this.RaiseAndSetIfChanged(ref _startDate, value);
        }
        public string EndDate
        {
            get => _endDate;
            set => this.RaiseAndSetIfChanged(ref _endDate, value);
        }
        public string Gpa
        {
            get => _gpa;
            set => this.RaiseAndSetIfChanged(ref _gpa, value);
        }
        public string Attending
        {
            get => _attending;
            set => this.RaiseAndSetIfChanged(ref _attending, value);
        }
        public ReactiveCommand<Unit, Unit> NewBtnPressed { get; }
        public ReactiveCommand<Unit, Unit> EditBtnPressed { get; }
        public ReactiveCommand<Unit, Unit> DeleteBtnPressed { get; }
        public ReactiveCommand<Unit, Unit> CompareBtnPressed { get; }

        public EducationViewModel()
        {
            _vault = (VaultData) Locator.Current.GetService(typeof(VaultData));
            _vaultPerson = (VaultPerson) Locator.Current.GetService(typeof(VaultPerson));
            NewBtnPressed = ReactiveCommand.Create(OnNewPressed);
            EditBtnPressed = ReactiveCommand.Create(OnEditPressed);
            DeleteBtnPressed = ReactiveCommand.Create(OnDeletePressed);
            CompareBtnPressed = ReactiveCommand.Create(OnComparePressed);
            Selection = new SelectionModel<IModel>();
            Selection.SelectionChanged += SelectionChanged!;
        }
        private async void OnNewPressed()
        {
            var queryModel = new QueryViewModel {View = ResumeView.Education, QueryType = QueryType.Insert};
            queryModel.QueryBuilder();
            var queryView = new QueryView {DataContext = queryModel};
            if (Application.Current.ApplicationLifetime is IClassicDesktopStyleApplicationLifetime desktop)
            {
                await queryView.ShowDialog(desktop.MainWindow);
                Populate();
            }
        }

        private async void OnEditPressed()
        {
            var queryModel = new QueryViewModel {View = ResumeView.Education, QueryType = QueryType.Update};
            queryModel.QueryBuilder(_selectedItem);
            var queryView = new QueryView {DataContext = queryModel};
            if (Application.Current.ApplicationLifetime is IClassicDesktopStyleApplicationLifetime desktop)
            {
                await queryView.ShowDialog(desktop.MainWindow);
                Populate();
            }
        }

        private async void OnDeletePressed()
        {
            var queryModel = new QueryViewModel {View = ResumeView.Education, QueryType = QueryType.Delete};
            queryModel.QueryBuilder(_selectedItem);
            var queryView = new QueryView {DataContext = queryModel};
            if (Application.Current.ApplicationLifetime is IClassicDesktopStyleApplicationLifetime desktop)
            {
                await queryView.ShowDialog(desktop.MainWindow);
                Populate();
            }
        }
        private async void OnComparePressed()
        {
            var compareModel = new CompareViewModel {View = ResumeView.Education};
            compareModel.ListBuilder(_educationList);
            if (_vaultPerson.Educations.Count > 0)
            {
                compareModel.RightList.Clear();
                foreach (var item in _vaultPerson.Educations)
                {
                    foreach (var edu in item.Value)
                    {
                        compareModel.RightList.Add(edu.Value);
                    }
                }
            }
            var compareView = new CompareView {DataContext = compareModel};
            if (Application.Current.ApplicationLifetime is IClassicDesktopStyleApplicationLifetime desktop)
            {
                await compareView.ShowDialog(desktop.MainWindow);
            }
        }
        void SelectionChanged(object sender, SelectionModelSelectionChangedEventArgs e)
        {
            SchoolName = EducationList[Selection.SelectedIndex].SchoolName;
            Major = EducationList[Selection.SelectedIndex].Major;
            Minor = EducationList[Selection.SelectedIndex].Minor;
            StartDate = EducationList[Selection.SelectedIndex].StartDate.ToString("yyyy-MM-dd");
            EndDate = EducationList[Selection.SelectedIndex].EndDate.ToString("yyyy-MM-dd");
            Gpa = EducationList[Selection.SelectedIndex].GPA.ToString("F2");
            Attending = "No";
            if (EducationList[Selection.SelectedIndex].Attending)
            {
                Attending = "Yes";
            }
        }

        public void Populate()
        {
            if (EducationList.Count > 0)
            {
                EducationList.Clear();
            }
            SchoolName = "";
            Major = "";
            Minor = "";
            StartDate = "";
            EndDate = "";
            Gpa = "";
            Attending = "";
            EducationModel model = new() {PersonID = _vaultPerson.Person.ID};
            EducationList = _vault.EducationProcessor.LoadDataListByPerson(model);
        }
    }
}