using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Reactive;
using Avalonia.Controls;
using ReactiveUI;
using ResumeVaultAvalonia.Models;
using Splat;
using ResumeVaultLib.Data.Models;

namespace ResumeVaultAvalonia.ViewModels
{
    public class CompareViewModel : ViewModelBase
    {
        private ObservableCollection<IModel> _leftList = null!, _rightList = null!;
        private VaultPerson _vaultPerson;
        private ResumeView _view;
        private IModel _leftSelectedItem = null!, _rightSelectedItem = null!;
        public ResumeView View
        {
            set => _view = value;
        }

        public ObservableCollection<IModel> LeftList
        {
            get => _leftList;
            set => this.RaiseAndSetIfChanged(ref _leftList, value);
        }
        public ObservableCollection<IModel> RightList
        {
            get => _rightList;
            set => this.RaiseAndSetIfChanged(ref _rightList, value);
        }

        public IModel LeftSelectedItem
        {
            get => _leftSelectedItem;
            set => this.RaiseAndSetIfChanged(ref _leftSelectedItem, value);
        }
        public IModel RightSelectedItem
        {
            get => _rightSelectedItem;
            set => this.RaiseAndSetIfChanged(ref _rightSelectedItem, value);
        }
        public ReactiveCommand<Unit, Unit> AddBtnPressed { get; }
        public ReactiveCommand<Unit, Unit> RemoveBtnPressed { get; }
        public ReactiveCommand<Unit, Unit> ClearBtnPressed { get; }
        public ReactiveCommand<Window, Unit> AcceptBtnPressed { get; }
        public ReactiveCommand<Window, Unit> RejectBtnPressed { get; }

        public CompareViewModel()
        {
            _vaultPerson = (VaultPerson) Locator.Current.GetService(typeof(VaultPerson));
            AddBtnPressed = ReactiveCommand.Create(OnAddPressed);
            RemoveBtnPressed = ReactiveCommand.Create(OnRemovePressed);
            ClearBtnPressed = ReactiveCommand.Create(OnClearPressed);
            AcceptBtnPressed = ReactiveCommand.Create<Window>(OnAcceptPressed);
            RejectBtnPressed = ReactiveCommand.Create<Window>(OnRejectPressed);
            RightList = new ObservableCollection<IModel>();
        }

        private void OnAddPressed()
        {
            RightList.Add(LeftSelectedItem);
        }

        private void OnRemovePressed()
        {
            RightList.Remove(RightSelectedItem);
        }

        private void OnClearPressed()
        {
            RightList.Clear();
        }

        private void OnAcceptPressed(Window window)
        {
            _vaultPerson.GetCategories();
            _vaultPerson.ClearDict(LeftList[0]);
            foreach (var item in RightList)
            {
                _vaultPerson.SetModelDictionary(item, true);
            }
            window.Close();
        }

        private void OnRejectPressed(Window window)
        {
            window.Close();
        }

        public void ListBuilder(object modelList)
        {
            switch (_view)
            {
                case ResumeView.Experience:
                    LeftList = new ObservableCollection<IModel>((List<ExperienceModel>)modelList);
                    break;
                case ResumeView.Education:
                    LeftList = new ObservableCollection<IModel>((List<EducationModel>) modelList);
                    break;
                case ResumeView.Skills:
                    LeftList = new ObservableCollection<IModel>((List<SkillsModel>) modelList);
                    break;
                case ResumeView.Misc:
                    LeftList = new ObservableCollection<IModel>((List<MiscModel>) modelList);
                    break;
            }
        }
    }
}