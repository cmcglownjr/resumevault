using System.Collections.Generic;
using System.Reactive;
using Avalonia;
using Avalonia.Controls.ApplicationLifetimes;
using Avalonia.Controls.Selection;
using ReactiveUI;
using ResumeVaultAvalonia.Models;
using ResumeVaultAvalonia.Views;
using ResumeVaultLib.Data.Models;
using ResumeVaultLib.Data.Utilities;
using Splat;

namespace ResumeVaultAvalonia.ViewModels
{
    public class ExperienceViewModel : ViewModelBase
    {
        private List<ExperienceModel> _experienceList = new();
        private VaultData _vault;
        private VaultPerson _vaultPerson;
        private ExperienceModel _selectedItem = null!;
        private string _organization = null!, _title = null!, _salary1 = null!, _salary2 = null!, _experienceDetails = null!, _date1 = null!, _date2 = null!, _current = null!;
        public List<ExperienceModel> ExperienceList
        {
            get => _experienceList;
            set => this.RaiseAndSetIfChanged(ref _experienceList, value);
        }
        public SelectionModel<IModel> Selection { get; }

        public ExperienceModel SelectedItem
        {
            get => _selectedItem;
            set => this.RaiseAndSetIfChanged(ref _selectedItem, value);
        }
        public string Organization
        {
            get => _organization;
            set => this.RaiseAndSetIfChanged(ref _organization, value);
        }
        public string Title
        {
            get => _title;
            set => this.RaiseAndSetIfChanged(ref _title, value);
        }
        public string Salary1
        {
            get => _salary1;
            set => this.RaiseAndSetIfChanged(ref _salary1, value);
        }
        public string Salary2
        {
            get => _salary2;
            set => this.RaiseAndSetIfChanged(ref _salary2, value);
        }
        public string ExperienceDetails
        {
            get => _experienceDetails;
            set => this.RaiseAndSetIfChanged(ref _experienceDetails, value);
        }
        public string Date1
        {
            get => _date1;
            set => this.RaiseAndSetIfChanged(ref _date1, value);
        }
        public string Date2
        {
            get => _date2;
            set => this.RaiseAndSetIfChanged(ref _date2, value);
        }
        public string CurrentExperience
        {
            get => _current;
            set => this.RaiseAndSetIfChanged(ref _current, value);
        }
        public ReactiveCommand<Unit, Unit> NewBtnPressed { get; }
        public ReactiveCommand<Unit, Unit> EditBtnPressed { get; }
        public ReactiveCommand<Unit, Unit> DeleteBtnPressed { get; }
        public ReactiveCommand<Unit, Unit> CompareBtnPressed { get; }

        public ExperienceViewModel()
        {
            _vault = (VaultData) Locator.Current.GetService(typeof(VaultData));
            _vaultPerson = (VaultPerson) Locator.Current.GetService(typeof(VaultPerson));
            NewBtnPressed = ReactiveCommand.Create(OnNewPressed);
            EditBtnPressed = ReactiveCommand.Create(OnEditPressed);
            DeleteBtnPressed = ReactiveCommand.Create(OnDeletePressed);
            CompareBtnPressed = ReactiveCommand.Create(OnComparePressed);
            Selection = new SelectionModel<IModel>();
            Selection.SelectionChanged += SelectionChanged!;
        }
        private async void OnNewPressed()
        {
            var queryModel = new QueryViewModel {View = ResumeView.Experience, QueryType = QueryType.Insert};
            queryModel.QueryBuilder();
            var queryView = new QueryView {DataContext = queryModel};
            if (Application.Current.ApplicationLifetime is IClassicDesktopStyleApplicationLifetime desktop)
            {
                await queryView.ShowDialog(desktop.MainWindow);
                Populate();
            }
        }

        private async void OnEditPressed()
        {
            var queryModel = new QueryViewModel {View = ResumeView.Experience, QueryType = QueryType.Update};
            queryModel.QueryBuilder(_selectedItem);
            var queryView = new QueryView {DataContext = queryModel};
            if (Application.Current.ApplicationLifetime is IClassicDesktopStyleApplicationLifetime desktop)
            {
                await queryView.ShowDialog(desktop.MainWindow);
                Populate();
            }
        }

        private async void OnDeletePressed()
        {
            var queryModel = new QueryViewModel {View = ResumeView.Experience, QueryType = QueryType.Delete};
            queryModel.QueryBuilder(_selectedItem);
            var queryView = new QueryView {DataContext = queryModel};
            if (Application.Current.ApplicationLifetime is IClassicDesktopStyleApplicationLifetime desktop)
            {
                await queryView.ShowDialog(desktop.MainWindow);
                Populate();
            }
        }

        private async void OnComparePressed()
        {
            var compareModel = new CompareViewModel {View = ResumeView.Experience};
            compareModel.ListBuilder(_experienceList);
            if (_vaultPerson.Experiences.Count > 0)
            {
                compareModel.RightList.Clear();
                foreach (var item in _vaultPerson.Experiences)
                {
                    foreach (var edu in item.Value)
                    {
                        compareModel.RightList.Add(edu.Value);
                    }
                }
            }
            var compareView = new CompareView {DataContext = compareModel};
            if (Application.Current.ApplicationLifetime is IClassicDesktopStyleApplicationLifetime desktop)
            {
                await compareView.ShowDialog(desktop.MainWindow);
            }
        }
        void SelectionChanged(object sender, SelectionModelSelectionChangedEventArgs e)
        {
            Organization = ExperienceList[Selection.SelectedIndex].Organization;
            Title = ExperienceList[Selection.SelectedIndex].Title;
            Date1 = ExperienceList[Selection.SelectedIndex].StartDate.ToString("yyyy-MM-dd");
            Date2 = ExperienceList[Selection.SelectedIndex].EndDate.ToString("yyyy-MM-dd");
            Salary1 = ExperienceList[Selection.SelectedIndex].StartSalary.ToString("C");
            Salary2 = ExperienceList[Selection.SelectedIndex].EndSalary.ToString("C");
            ExperienceDetails = ExperienceList[Selection.SelectedIndex].Details;
            CurrentExperience = "No";
            if (ExperienceList[Selection.SelectedIndex].Current)
            {
                CurrentExperience = "Yes";
            }
        }
        public void Populate()
        {
            if (ExperienceList.Count > 0)
            {
                ExperienceList.Clear();
            }

            Organization = "";
            Title = "";
            Date1 = "";
            Date2 = "";
            Salary1 = "";
            Salary2 = "";
            ExperienceDetails = "";
            CurrentExperience = "";
            ExperienceModel model = new() {PersonID = _vaultPerson.Person.ID};
            ExperienceList = _vault.ExperienceProcessor.LoadDataListByPerson(model);
        }
    }
}