using System.Collections.Generic;
using System.Reactive;
using Avalonia;
using Avalonia.Controls.ApplicationLifetimes;
using Avalonia.Controls.Selection;
using ReactiveUI;
using ResumeVaultAvalonia.Models;
using ResumeVaultAvalonia.Views;
using ResumeVaultLib.Data.Models;
using ResumeVaultLib.Data.Utilities;
using Splat;

namespace ResumeVaultAvalonia.ViewModels
{
    public class AddressViewModel : ViewModelBase
    {
        private List<AddressModel> _addressList = new();
        private VaultData _vault;
        private VaultPerson _vaultPerson;
        private AddressModel _selectedItem = null!;
        private string _addressName = null!, _address1 = null!, _address2 = null!, _city = null!, _state = null!, _zip = null!;
        private Dictionary<int, string> _states = new();
        public List<AddressModel> AddressList
        {
            get => _addressList;
            set => this.RaiseAndSetIfChanged(ref _addressList, value);
        }
        public SelectionModel<IModel> Selection { get; }

        public AddressModel SelectedItem
        {
            get => _selectedItem;
            set => this.RaiseAndSetIfChanged(ref _selectedItem, value);
        }

        public string AddressName
        {
            get => _addressName;
            set => this.RaiseAndSetIfChanged(ref _addressName, value);
        }
        public string Address1
        {
            get => _address1;
            set => this.RaiseAndSetIfChanged(ref _address1, value);
        }
        public string Address2
        {
            get => _address2;
            set => this.RaiseAndSetIfChanged(ref _address2, value);
        }
        public string City
        {
            get => _city;
            set => this.RaiseAndSetIfChanged(ref _city, value);
        }
        public string State   
        {
            get => _state;
            set => this.RaiseAndSetIfChanged(ref _state, value);
        }
        public string Zip
        {
            get => _zip;
            set => this.RaiseAndSetIfChanged(ref _zip, value);
        }
        public ReactiveCommand<Unit, Unit> NewBtnPressed { get; }
        public ReactiveCommand<Unit, Unit> EditBtnPressed { get; }
        public ReactiveCommand<Unit, Unit> DeleteBtnPressed { get; }

        public AddressViewModel()
        {
            _vault = (VaultData) Locator.Current.GetService(typeof(VaultData));
            _vaultPerson = (VaultPerson) Locator.Current.GetService(typeof(VaultPerson));
            NewBtnPressed = ReactiveCommand.Create(OnNewPressed);
            EditBtnPressed = ReactiveCommand.Create(OnEditPressed);
            DeleteBtnPressed = ReactiveCommand.Create(OnDeletePressed);
            Selection = new SelectionModel<IModel>();
            Selection.SelectionChanged += SelectionChanged!;
        }
        private async void OnNewPressed()
        {
            var queryModel = new QueryViewModel {View = ResumeView.Address, QueryType = QueryType.Insert};
            queryModel.QueryBuilder();
            var queryView = new QueryView {DataContext = queryModel};
            if (Application.Current.ApplicationLifetime is IClassicDesktopStyleApplicationLifetime desktop)
            {
                await queryView.ShowDialog(desktop.MainWindow);
                Populate();
            }
        }

        private async void OnEditPressed()
        {
            var queryModel = new QueryViewModel {View = ResumeView.Address, QueryType = QueryType.Update};
            queryModel.QueryBuilder(_selectedItem);
            var queryView = new QueryView {DataContext = queryModel};
            if (Application.Current.ApplicationLifetime is IClassicDesktopStyleApplicationLifetime desktop)
            {
                await queryView.ShowDialog(desktop.MainWindow);
                Populate();
            }
        }

        private async void OnDeletePressed()
        {
            var queryModel = new QueryViewModel {View = ResumeView.Address, QueryType = QueryType.Delete};
            queryModel.QueryBuilder(_selectedItem);
            var queryView = new QueryView {DataContext = queryModel};
            if (Application.Current.ApplicationLifetime is IClassicDesktopStyleApplicationLifetime desktop)
            {
                await queryView.ShowDialog(desktop.MainWindow);
                Populate();
            }
        }
        void SelectionChanged(object sender, SelectionModelSelectionChangedEventArgs e)
        {
            _vaultPerson.Address = AddressList[Selection.SelectedIndex];
            AddressName = AddressList[Selection.SelectedIndex].Name;
            Address1 = AddressList[Selection.SelectedIndex].Address1;
            Address2 = AddressList[Selection.SelectedIndex].Address2;
            City = AddressList[Selection.SelectedIndex].City;
            State = _states[AddressList[Selection.SelectedIndex].StateProvence];
            Zip = AddressList[Selection.SelectedIndex].Zip;
        }

        public void Populate()
        {
            if (AddressList.Count > 0)
            {
                AddressList.Clear();
            }
            GetStatesProvence();
            ClearData();
            AddressModel model = new() {PersonID = _vaultPerson.Person.ID};
            AddressList = _vault.AddressProcessor.LoadDataListByPerson(model);
        }

        private void GetStatesProvence()
        {
            if (_states.Count > 0)
            {
                _states.Clear();
            }
            StateProvenceModel model = new();
            var results = _vault.StateProvenceProcessor.LoadAllDataList(model);
            foreach (var stateProvence in results)
            {
                _states.Add(stateProvence.ID, stateProvence.StateProvence);
            }
        }

        private void ClearData()
        {
            AddressName = "";
            Address1 = "";
            Address2 = "";
            City = "";
            State = "";
            Zip = "";
        }
    }
}