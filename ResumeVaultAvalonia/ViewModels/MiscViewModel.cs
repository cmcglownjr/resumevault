using System.Collections.Generic;
using Avalonia;
using Avalonia.Controls.ApplicationLifetimes;
using Avalonia.Controls.Selection;
using System.Reactive;
using ReactiveUI;
using ResumeVaultAvalonia.Models;
using ResumeVaultAvalonia.Views;
using ResumeVaultLib.Data.Models;
using ResumeVaultLib.Data.Utilities;
using Splat;

namespace ResumeVaultAvalonia.ViewModels
{
    public class MiscViewModel : ViewModelBase
    {
        private List<MiscModel> _miscList = new();
        private VaultData _vault;
        private VaultPerson _vaultPerson;
        private MiscModel _selectedItem = null!;
        private string _miscName = null!, _miscCategory = null!, _miscDate = null!, _miscDescription = null!;
        private Dictionary<int, string> _category = new();

        public List<MiscModel> MiscList
        {
            get => _miscList;
            set => this.RaiseAndSetIfChanged(ref _miscList, value);
        }
        public MiscModel SelectedItem
        {
            get => _selectedItem;
            set => this.RaiseAndSetIfChanged(ref _selectedItem, value);
        }
        public string MiscName
        {
            get => _miscName;
            set => this.RaiseAndSetIfChanged(ref _miscName, value);
        }
        public string MiscCategory
        {
            get => _miscCategory;
            set => this.RaiseAndSetIfChanged(ref _miscCategory, value);
        }
        public string MiscDate
        {
            get => _miscDate;
            set => this.RaiseAndSetIfChanged(ref _miscDate, value);
        }
        public string MiscDescription
        {
            get => _miscDescription;
            set => this.RaiseAndSetIfChanged(ref _miscDescription, value);
        }
        public SelectionModel<IModel> Selection { get; }
        public ReactiveCommand<Unit, Unit> NewBtnPressed { get; }
        public ReactiveCommand<Unit, Unit> EditBtnPressed { get; }
        public ReactiveCommand<Unit, Unit> DeleteBtnPressed { get; }
        public ReactiveCommand<Unit, Unit> CompareBtnPressed { get; }

        public MiscViewModel()
        {
            _vault = (VaultData) Locator.Current.GetService(typeof(VaultData));
            _vaultPerson = (VaultPerson) Locator.Current.GetService(typeof(VaultPerson));
            NewBtnPressed = ReactiveCommand.Create(OnNewPressed);
            EditBtnPressed = ReactiveCommand.Create(OnEditPressed);
            DeleteBtnPressed = ReactiveCommand.Create(OnDeletePressed);
            CompareBtnPressed = ReactiveCommand.Create(OnComparePressed);
            Selection = new SelectionModel<IModel>();
            Selection.SelectionChanged += SelectionChanged!;
        }
        private async void OnNewPressed()
        {
            var queryModel = new QueryViewModel {View = ResumeView.Misc, QueryType = QueryType.Insert};
            queryModel.QueryBuilder();
            var queryView = new QueryView {DataContext = queryModel};
            if (Application.Current.ApplicationLifetime is IClassicDesktopStyleApplicationLifetime desktop)
            {
                await queryView.ShowDialog(desktop.MainWindow);
                Populate();
            }
        }

        private async void OnEditPressed()
        {
            var queryModel = new QueryViewModel {View = ResumeView.Misc, QueryType = QueryType.Update};
            queryModel.QueryBuilder(_selectedItem);
            var queryView = new QueryView {DataContext = queryModel};
            if (Application.Current.ApplicationLifetime is IClassicDesktopStyleApplicationLifetime desktop)
            {
                await queryView.ShowDialog(desktop.MainWindow);
                Populate();
            }
        }

        private async void OnDeletePressed()
        {
            var queryModel = new QueryViewModel {View = ResumeView.Misc, QueryType = QueryType.Delete};
            queryModel.QueryBuilder(_selectedItem);
            var queryView = new QueryView {DataContext = queryModel};
            if (Application.Current.ApplicationLifetime is IClassicDesktopStyleApplicationLifetime desktop)
            {
                await queryView.ShowDialog(desktop.MainWindow);
                Populate();
            }
        }
        private async void OnComparePressed()
        {
            var compareModel = new CompareViewModel {View = ResumeView.Misc};
            compareModel.ListBuilder(_miscList);
            if (_vaultPerson.Misc.Count > 0)
            {
                compareModel.RightList.Clear();
                foreach (var item in _vaultPerson.Misc)
                {
                    foreach (var edu in item.Value)
                    {
                        compareModel.RightList.Add(edu.Value);
                    }
                }
            }
            var compareView = new CompareView {DataContext = compareModel};
            if (Application.Current.ApplicationLifetime is IClassicDesktopStyleApplicationLifetime desktop)
            {
                await compareView.ShowDialog(desktop.MainWindow);
            }
        }
        void SelectionChanged(object sender, SelectionModelSelectionChangedEventArgs e)
        {
            MiscName = MiscList[Selection.SelectedIndex].Name;
            MiscCategory = _category[MiscList[Selection.SelectedIndex].Category];
            MiscDate = MiscList[Selection.SelectedIndex].Date.ToString("yyyy-MM-dd");
            MiscDescription = MiscList[Selection.SelectedIndex].Description;
        }

        public void Populate()
        {
            if (MiscList.Count > 0)
            {
                MiscList.Clear();
            }
            GetCategories();
            MiscName = "";
            MiscCategory = "";
            MiscDate = "";
            MiscModel model = new() {PersonID = _vaultPerson.Person.ID};
            MiscList = _vault.MiscProcessor.LoadDataListByPerson(model);
        }

        private void GetCategories()
        {
            if (_category.Count > 0)
            {
                _category.Clear();
            }

            MiscCategoryModel model = new();
            var results = _vault.MiscCategoryProcessor.LoadAllDataList(model);
            foreach (var category in results)
            {
                _category.Add(category.ID, category.Name);
            }
        }
    }
}