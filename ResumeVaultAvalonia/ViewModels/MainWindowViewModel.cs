﻿using System.IO;
using System.Reactive;
using System.Threading.Tasks;
using Avalonia.Controls;
using MessageBox.Avalonia.Enums;
using ReactiveUI;
using ResumeVaultAvalonia.Models;
using ResumeVaultAvalonia.Views;
using Splat;

namespace ResumeVaultAvalonia.ViewModels
{
    public class MainWindowViewModel : ViewModelBase
    {
        private readonly MainWindow _mainWindow;
        private readonly PersonViewModel _personViewModel;
        private readonly VaultData _vault;
        private readonly VaultPerson _vaultPerson;
        private bool _greetingVisible, _personVisible, _bodyVisible;
        public bool GreetingVisible
        {
            get => _greetingVisible;
            set => this.RaiseAndSetIfChanged(ref _greetingVisible, value);
        } 
        public bool PersonVisible
        {
            get => _personVisible;
            set => this.RaiseAndSetIfChanged(ref _personVisible, value);
        } 
        public bool BodyVisible
        {
            get => _bodyVisible;
            set => this.RaiseAndSetIfChanged(ref _bodyVisible, value);
        } 
        public ReactiveCommand<Unit, Unit> NewMenuBtnPressed { get; }
        public ReactiveCommand<Unit, Unit> OpenMenuBtnPressed { get; }
        public ReactiveCommand<Unit, Unit> PrintDocumentBtnPressed { get; }
        public ReactiveCommand<Window, Unit> ExitBtnPressed { get; }
        public ReactiveCommand<Unit, Unit> AboutBtnPressed { get; }
        public MainWindowViewModel()
        {
            _mainWindow = new MainWindow();
            _vault = (VaultData) Locator.Current.GetService(typeof(VaultData));
            _vaultPerson = (VaultPerson) Locator.Current.GetService(typeof(VaultPerson));
            _personViewModel = (PersonViewModel) Locator.Current.GetService(typeof(PersonViewModel));
            NewMenuBtnPressed = ReactiveCommand.CreateFromTask(NewDatabase);
            OpenMenuBtnPressed = ReactiveCommand.CreateFromTask(OpenDatabase);
            PrintDocumentBtnPressed = ReactiveCommand.CreateFromTask(PrintWordDoc);
            ExitBtnPressed = ReactiveCommand.Create<Window>(ExitClick);
            AboutBtnPressed = ReactiveCommand.Create(AboutClicked);
            GreetingVisible = true;
            PersonVisible = false;
            BodyVisible = false;
        }

        private async Task NewDatabase()
        {
            SaveFileDialog dialog = new();
            dialog.Filters.Add(new FileDialogFilter{Name = "Database (*.db)", Extensions = {"db"}});
            dialog.Title = "Create New Database";
            string result = await dialog.ShowAsync(_mainWindow);
            if (result != null)
            {
               FileInfo db = _vault.DatabaseAccess.CreateDatabase(Path.GetFileName(result), 
                   Path.GetDirectoryName(result));
               _vault.DatabaseAccess.LoadDatabase(db);
               GreetingVisible = false;
               PersonVisible = true;
               BodyVisible = true;
            }
        }

        private async Task OpenDatabase()
        {
            OpenFileDialog dialog = new();
            dialog.Filters.Add(new FileDialogFilter{Name = "Database (*.db)", Extensions = {"db"}});
            dialog.AllowMultiple = false;
            dialog.Title = "Open Existing Database";
            string[] result = await dialog.ShowAsync(_mainWindow);
            if (result != null)
            {
                foreach (string filepath in result)
                {
                    FileInfo db = new(filepath);
                    _vault.DatabaseAccess.LoadDatabase(db);
                    GreetingVisible = false;
                    PersonVisible = true;
                }
            }
            _personViewModel.PopulatePerson();
            _personViewModel.Selection.Select(0);
            BodyVisible = true;
        }

        private async Task PrintWordDoc()
        {
            SaveFileDialog dialog = new();
            dialog.Filters.Add(new FileDialogFilter{Name = "Word Document (*.docx)", Extensions = {"docx"}});
            dialog.Title = "Save Word Document";
            string result = await dialog.ShowAsync(_mainWindow);
            if (result != null)
            {
                FileInfo filename = new(Path.Combine(Path.GetDirectoryName(result) ?? string.Empty, Path.GetFileName(result)));
                if (ResumeCheck())
                {
                    _vaultPerson.GenerateDocument(filename);
                    Models.MessageBox.ResumeMessageBoxDialog("Success!", "Document created!", Icon.Success);
                }
                else
                {
                    Models.MessageBox.ResumeMessageBoxDialog("Error!", "You need to select a person!", Icon.Error);
                }
            }
        }

        private void ExitClick(Window window)
        {
            window.Close();
        }

        private void AboutClicked()
        {
            Models.MessageBox.ResumeMessageBoxDialog("About", "This is a portfolio project. I wanted to learn" +
                                                              " about database connection, GUI development, and some other" +
                                                              " stuff.", Icon.Info);
        }

        private bool ResumeCheck()
        {
            if (_vaultPerson.Person.FirstName is null)
            {
                return false;
            }

            return true;
        }
    }
}