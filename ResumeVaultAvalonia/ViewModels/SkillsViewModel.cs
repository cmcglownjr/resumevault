using System.Collections.Generic;
using Avalonia;
using Avalonia.Controls.ApplicationLifetimes;
using Avalonia.Controls.Selection;
using System.Reactive;
using ReactiveUI;
using ResumeVaultAvalonia.Models;
using ResumeVaultAvalonia.Views;
using ResumeVaultLib.Data.Models;
using ResumeVaultLib.Data.Utilities;
using Splat;

namespace ResumeVaultAvalonia.ViewModels
{
    public class SkillsViewModel : ViewModelBase
    {
        private List<SkillsModel> _skillsList = new();
        private VaultData _vault;
        private VaultPerson _vaultPerson;
        private SkillsModel _selectedItem = null!;
        private string _skillName = null!, _skillCategory = null!, _startDate = null!;
        private Dictionary<int, string> _category = new();
        public List<SkillsModel> SkillsList
        {
            get => _skillsList;
            set => this.RaiseAndSetIfChanged(ref _skillsList, value);
        }

        public SkillsModel SelectedItem
        {
            get => _selectedItem;
            set => this.RaiseAndSetIfChanged(ref _selectedItem, value);
        }

        public string SkillName
        {
            get => _skillName;
            set => this.RaiseAndSetIfChanged(ref _skillName, value);
        }
        public string SkillCategory
        {
            get => _skillCategory;
            set => this.RaiseAndSetIfChanged(ref _skillCategory, value);
        }
        public string StartDate
        {
            get => _startDate;
            set => this.RaiseAndSetIfChanged(ref _startDate, value);
        }
        public SelectionModel<IModel> Selection { get; }
        public ReactiveCommand<Unit, Unit> NewBtnPressed { get; }
        public ReactiveCommand<Unit, Unit> EditBtnPressed { get; }
        public ReactiveCommand<Unit, Unit> DeleteBtnPressed { get; }
        public ReactiveCommand<Unit, Unit> CompareBtnPressed { get; }

        public SkillsViewModel()
        {
            _vault = (VaultData) Locator.Current.GetService(typeof(VaultData));
            _vaultPerson = (VaultPerson) Locator.Current.GetService(typeof(VaultPerson));
            NewBtnPressed = ReactiveCommand.Create(OnNewPressed);
            EditBtnPressed = ReactiveCommand.Create(OnEditPressed);
            DeleteBtnPressed = ReactiveCommand.Create(OnDeletePressed);
            CompareBtnPressed = ReactiveCommand.Create(OnComparePressed);
            Selection = new SelectionModel<IModel>();
            Selection.SelectionChanged += SelectionChanged!;
        }
        private async void OnNewPressed()
        {
            var queryModel = new QueryViewModel {View = ResumeView.Skills, QueryType = QueryType.Insert};
            queryModel.QueryBuilder();
            var queryView = new QueryView {DataContext = queryModel};
            if (Application.Current.ApplicationLifetime is IClassicDesktopStyleApplicationLifetime desktop)
            {
                await queryView.ShowDialog(desktop.MainWindow);
                Populate();
            }
        }

        private async void OnEditPressed()
        {
            var queryModel = new QueryViewModel {View = ResumeView.Skills, QueryType = QueryType.Update};
            queryModel.QueryBuilder(_selectedItem);
            var queryView = new QueryView {DataContext = queryModel};
            if (Application.Current.ApplicationLifetime is IClassicDesktopStyleApplicationLifetime desktop)
            {
                await queryView.ShowDialog(desktop.MainWindow);
                Populate();
            }
        }

        private async void OnDeletePressed()
        {
            var queryModel = new QueryViewModel {View = ResumeView.Skills, QueryType = QueryType.Delete};
            queryModel.QueryBuilder(_selectedItem);
            var queryView = new QueryView {DataContext = queryModel};
            if (Application.Current.ApplicationLifetime is IClassicDesktopStyleApplicationLifetime desktop)
            {
                await queryView.ShowDialog(desktop.MainWindow);
                Populate();
            }
        }
        private async void OnComparePressed()
        {
            var compareModel = new CompareViewModel {View = ResumeView.Skills};
            compareModel.ListBuilder(_skillsList);
            if (_vaultPerson.Skills.Count > 0)
            {
                compareModel.RightList.Clear();
                foreach (var item in _vaultPerson.Skills)
                {
                    foreach (var edu in item.Value)
                    {
                        compareModel.RightList.Add(edu.Value);
                    }
                }
            }
            var compareView = new CompareView {DataContext = compareModel};
            if (Application.Current.ApplicationLifetime is IClassicDesktopStyleApplicationLifetime desktop)
            {
                await compareView.ShowDialog(desktop.MainWindow);
            }
        }

        void SelectionChanged(object sender, SelectionModelSelectionChangedEventArgs e)
        {
            SkillName = SkillsList[Selection.SelectedIndex].Name;
            SkillCategory = _category[SkillsList[Selection.SelectedIndex].Category];
            StartDate = SkillsList[Selection.SelectedIndex].StartDate.ToString("yyyy-MM-dd");
        }

        public void Populate()
        {
            if (SkillsList.Count > 0)
            {
                SkillsList.Clear();
            }
            GetCategories();
            SkillName = "";
            SkillCategory = "";
            StartDate = "";
            SkillsModel model = new() {PersonID = _vaultPerson.Person.ID};
            SkillsList = _vault.SkillsProcessor.LoadDataListByPerson(model);
        }

        private void GetCategories()
        {
            if (_category.Count > 0)
            {
                _category.Clear();
            }

            SkillCategoryModel model = new();
            var results = _vault.SkillCategoryProcessor.LoadAllDataList(model);
            foreach (var category in results)
            {
                _category.Add(category.ID, category.Name);
            }
        }
    }
}