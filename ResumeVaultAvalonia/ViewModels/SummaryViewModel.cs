using System.Collections.Generic;
using System.Reactive;
using Avalonia;
using Avalonia.Controls.ApplicationLifetimes;
using Avalonia.Controls.Selection;
using ReactiveUI;
using ResumeVaultAvalonia.Models;
using ResumeVaultAvalonia.Views;
using ResumeVaultLib.Data.Models;
using ResumeVaultLib.Data.Utilities;
using Splat;

namespace ResumeVaultAvalonia.ViewModels
{
    public class SummaryViewModel : ViewModelBase
    {
        private List<SummaryModel> _summaryList = new();
        private VaultData _vault;
        private VaultPerson _vaultPerson;
        private SummaryModel _selectedItem = null!;
        private string _summaryContent = null!, _summaryName = null!;

        public List<SummaryModel> SummaryList
        {
            get => _summaryList;
            set => this.RaiseAndSetIfChanged(ref _summaryList, value);
        }
        public SelectionModel<SummaryModel> Selection { get; }

        public SummaryModel SelectedItem
        {
            get => _selectedItem;
            set => this.RaiseAndSetIfChanged(ref _selectedItem, value);
        }

        public string SummaryContent
        {
            get => _summaryContent;
            set => this.RaiseAndSetIfChanged(ref _summaryContent, value);
        }
        public string SummaryName
        {
            get => _summaryName;
            set => this.RaiseAndSetIfChanged(ref _summaryName, value);
        }
        public ReactiveCommand<Unit, Unit> NewBtnPressed { get; }
        public ReactiveCommand<Unit, Unit> EditBtnPressed { get; }
        public ReactiveCommand<Unit, Unit> DeleteBtnPressed { get; }
        public SummaryViewModel()
        {
            _vault = (VaultData) Locator.Current.GetService(typeof(VaultData));
            _vaultPerson = (VaultPerson) Locator.Current.GetService(typeof(VaultPerson));
            NewBtnPressed = ReactiveCommand.Create(OnNewPressed);
            EditBtnPressed = ReactiveCommand.Create(OnEditPressed);
            DeleteBtnPressed = ReactiveCommand.Create(OnDeletePressed);
            Selection = new SelectionModel<SummaryModel>();
            Selection.SelectionChanged += SelectionChanged!;
        }
        private async void OnNewPressed()
        {
            var queryModel = new QueryViewModel {View = ResumeView.Summary, QueryType = QueryType.Insert};
            queryModel.QueryBuilder();
            var queryView = new QueryView {DataContext = queryModel};
            if (Application.Current.ApplicationLifetime is IClassicDesktopStyleApplicationLifetime desktop)
            {
                await queryView.ShowDialog(desktop.MainWindow);
                Populate();
            }
        }

        private async void OnEditPressed()
        {
            var queryModel = new QueryViewModel {View = ResumeView.Summary, QueryType = QueryType.Update};
            queryModel.QueryBuilder(_selectedItem);
            var queryView = new QueryView {DataContext = queryModel};
            if (Application.Current.ApplicationLifetime is IClassicDesktopStyleApplicationLifetime desktop)
            {
                await queryView.ShowDialog(desktop.MainWindow);
                Populate();
            }
        }

        private async void OnDeletePressed()
        {
            var queryModel = new QueryViewModel {View = ResumeView.Summary, QueryType = QueryType.Delete};
            queryModel.QueryBuilder(_selectedItem);
            var queryView = new QueryView {DataContext = queryModel};
            if (Application.Current.ApplicationLifetime is IClassicDesktopStyleApplicationLifetime desktop)
            {
                await queryView.ShowDialog(desktop.MainWindow);
                Populate();
            }
        }

        void SelectionChanged(object sender, SelectionModelSelectionChangedEventArgs e)
        {
            _vaultPerson.Summary = SummaryList[Selection.SelectedIndex];
            SummaryName = SummaryList[Selection.SelectedIndex].Name;
            SummaryContent = SummaryList[Selection.SelectedIndex].Description;
        }

        public void Populate()
        {
            if (SummaryList.Count > 0)
            {
                SummaryList.Clear();
            }

            SummaryName = "";
            SummaryContent = "";
            SummaryModel model = new() {PersonID = _vaultPerson.Person.ID};
            SummaryList = _vault.SummaryProcessor.LoadDataListByPerson(model);
        }
    }
}