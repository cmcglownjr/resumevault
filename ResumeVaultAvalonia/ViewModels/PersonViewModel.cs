using System.Collections.Generic;
using System.Reactive;
using Avalonia;
using Avalonia.Controls.ApplicationLifetimes;
using Avalonia.Controls.Selection;
using ReactiveUI;
using ResumeVaultAvalonia.Models;
using Splat;
using ResumeVaultAvalonia.Views;
using ResumeVaultLib.Data.Models;
using ResumeVaultLib.Data.Utilities;

namespace ResumeVaultAvalonia.ViewModels
{
    public class PersonViewModel : ViewModelBase
    {
        private List<PersonModel> _personList = new();
        private VaultData _vault;
        private VaultPerson _vaultPerson;
        private PersonModel _selectedItem = null!;
        private string _personName = null!, _contactNumber = null!, _email = null!, _linkedIn = null!;
        private AddressViewModel _address;
        private EducationViewModel _education;
        private ExperienceViewModel _experience;
        private MiscViewModel _misc;
        private SkillsViewModel _skills;
        private SummaryViewModel _summary;
        private TagViewModel _tag;
        public List<PersonModel> PersonList
        {
            get => _personList;
            set => this.RaiseAndSetIfChanged(ref _personList, value);
        }
        public SelectionModel<IModel> Selection { get; }

        public PersonModel SelectedItem
        {
            get => _selectedItem;
            set => this.RaiseAndSetIfChanged(ref _selectedItem, value);
        }
        public string PersonName
        {
            get => _personName;
            set => this.RaiseAndSetIfChanged(ref _personName, value);
        }
        public string ContactNumber
        {
            get => _contactNumber;
            set => this.RaiseAndSetIfChanged(ref _contactNumber, value);
        }
        public string Email
        {
            get => _email;
            set => this.RaiseAndSetIfChanged(ref _email, value);
        }

        public string LinkedIn
        {
            get => _linkedIn;
            set => this.RaiseAndSetIfChanged(ref _linkedIn, value);
        }
        public ReactiveCommand<Unit, Unit> NewBtnPressed { get; }
        public ReactiveCommand<Unit, Unit> EditBtnPressed { get; }
        public ReactiveCommand<Unit, Unit> DeleteBtnPressed { get; }

        public PersonViewModel()
        {
            _vault = (VaultData) Locator.Current.GetService(typeof(VaultData));
            _vaultPerson = (VaultPerson) Locator.Current.GetService(typeof(VaultPerson));
            _address = (AddressViewModel) Locator.Current.GetService(typeof(AddressViewModel));
            _education = (EducationViewModel) Locator.Current.GetService(typeof(EducationViewModel));
            _experience = (ExperienceViewModel) Locator.Current.GetService(typeof(ExperienceViewModel));
            _misc = (MiscViewModel) Locator.Current.GetService(typeof(MiscViewModel));
            _skills = (SkillsViewModel) Locator.Current.GetService(typeof(SkillsViewModel));
            _summary = (SummaryViewModel) Locator.Current.GetService(typeof(SummaryViewModel));
            _tag = (TagViewModel) Locator.Current.GetService(typeof(TagViewModel));
            NewBtnPressed = ReactiveCommand.Create(OnNewPressed);
            EditBtnPressed = ReactiveCommand.Create(OnEditPressed);
            DeleteBtnPressed = ReactiveCommand.Create(OnDeletePressed);
            Selection = new SelectionModel<IModel>();
            Selection.SelectionChanged += SelectionChanged!;
        }

        private async void OnNewPressed()
        {
            var queryModel = new QueryViewModel {View = ResumeView.Person, QueryType = QueryType.Insert};
            queryModel.QueryBuilder();
            var queryView = new QueryView {DataContext = queryModel};
            if (Application.Current.ApplicationLifetime is IClassicDesktopStyleApplicationLifetime desktop)
            {
                await queryView.ShowDialog(desktop.MainWindow);
                PopulatePerson();
            }
        }

        private async void OnEditPressed()
        {
            var queryModel = new QueryViewModel {View = ResumeView.Person, QueryType = QueryType.Update};
            queryModel.QueryBuilder(_vaultPerson.Person);
            var queryView = new QueryView {DataContext = queryModel};
            if (Application.Current.ApplicationLifetime is IClassicDesktopStyleApplicationLifetime desktop)
            {
                await queryView.ShowDialog(desktop.MainWindow);
                PopulatePerson();
            }
        }

        private async void OnDeletePressed()
        {
            var queryModel = new QueryViewModel {View = ResumeView.Person, QueryType = QueryType.Delete};
            queryModel.QueryBuilder(_vaultPerson.Person);
            var queryView = new QueryView {DataContext = queryModel};
            if (Application.Current.ApplicationLifetime is IClassicDesktopStyleApplicationLifetime desktop)
            {
                await queryView.ShowDialog(desktop.MainWindow);
                PopulatePerson();
            }
        }

        void SelectionChanged(object sender, SelectionModelSelectionChangedEventArgs e)
        {
            _vaultPerson.Person = PersonList[Selection.SelectedIndex];
            PersonName = PersonList[Selection.SelectedIndex].FullName;
            ContactNumber = PersonList[Selection.SelectedIndex].ContactNumber;
            Email = PersonList[Selection.SelectedIndex].ContactEmail;
            LinkedIn = PersonList[Selection.SelectedIndex].LinkedIn;
            _address.Populate();
            _summary.Populate();
            _education.Populate();
            _experience.Populate();
            _skills.Populate();
            _misc.Populate();
            _tag.Populate();
        }

        public void PopulatePerson()
        {
            PersonList = _vault.GeneratePeople();
        }
    }
}