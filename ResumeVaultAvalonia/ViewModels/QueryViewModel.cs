using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive;
using Avalonia.Controls;
using ReactiveUI;
using ResumeVaultAvalonia.Models;
using Splat;
using ResumeVaultLib.Data.Models;
using ResumeVaultLib.Data.Utilities;

namespace ResumeVaultAvalonia.ViewModels
{
    public class QueryViewModel : ViewModelBase
    {
        private string _text1Label = null!, _text2Label = null!, _text3Label = null!, _text4Label = null!, _text5Label = null!, _lgTextLabel = null!, _date1Label = null!, _date2Label = null!;
        private string _comboLabel = null!, _checkboxLabel = null!, _queryButtonLabel = null!, _header = null!;
        private string _textBox1Input = null!, _textBox2Input = null!, _textBox3Input = null!, _textBox4Input = null!, _textBox5Input = null!, _lgTextBoxInput = null!;
        private bool _textBox1Visible, _textBox2Visible, _textBox3Visible, _textBox4Visible, _textBox5Visible,  
            _date1Visible, _date2Visible, _comboBoxVisible, _checkBoxVisible, _lgTextBoxVisible, _editEnable;
        private string _borderThickness;
        private DateTime _date1, _date2;
        private KeyValuePair<int, string> _selectedItem;
        private bool _chkBox;
        private ResumeView _view;
        private QueryType _type;
        private IModel _model = null!;
        private VaultData _vaultData;
        private VaultPerson _vaultPerson;
        private Dictionary<int, string> _comboItems = new();

        public QueryType QueryType
        {
            set => _type = value;
        }
        public ResumeView View
        {
            set => _view = value;
        }

        public Dictionary<int, string> ComboItems
        {
            get => _comboItems;
            set => this.RaiseAndSetIfChanged(ref _comboItems, value);
        }

        public KeyValuePair<int, string> SelectedItem
        {
            get => _selectedItem;
            set => this.RaiseAndSetIfChanged(ref _selectedItem, value);
        }
        public string Header
        {
            get => _header;
            set => this.RaiseAndSetIfChanged(ref _header, value);
        }
        public string Text1Label
        {
            get => _text1Label;
            set => this.RaiseAndSetIfChanged(ref _text1Label, value);
        }
        public string Text2Label
        {
            get => _text2Label;
            set => this.RaiseAndSetIfChanged(ref _text2Label, value);
        }
        public string Text3Label
        {
            get => _text3Label;
            set => this.RaiseAndSetIfChanged(ref _text3Label, value);
        }
        public string Text4Label
        {
            get => _text4Label;
            set => this.RaiseAndSetIfChanged(ref _text4Label, value);
        }
        public string Text5Label
        {
            get => _text5Label;
            set => this.RaiseAndSetIfChanged(ref _text5Label, value);
        }
        public string LgTextLabel
        {
            get => _lgTextLabel;
            set => this.RaiseAndSetIfChanged(ref _lgTextLabel, value);
        }
        public string Date1Label
        {
            get => _date1Label;
            set => this.RaiseAndSetIfChanged(ref _date1Label, value);
        }
        public string Date2Label
        {
            get => _date2Label;
            set => this.RaiseAndSetIfChanged(ref _date2Label, value);
        }
        public string ComboLabel
        {
            get => _comboLabel;
            set => this.RaiseAndSetIfChanged(ref _comboLabel, value);
        }
        public string CheckboxLabel
        {
            get => _checkboxLabel;
            set => this.RaiseAndSetIfChanged(ref _checkboxLabel, value);
        }
        public string QueryButtonLabel
        {
            get => _queryButtonLabel;
            set => this.RaiseAndSetIfChanged(ref _queryButtonLabel, value);
        }
        public string TextBox1Input
        {
            get => _textBox1Input;
            set => this.RaiseAndSetIfChanged(ref _textBox1Input, value);
        }
        public string TextBox2Input
        {
            get => _textBox2Input;
            set => this.RaiseAndSetIfChanged(ref _textBox2Input, value);
        }
        public string TextBox3Input
        {
            get => _textBox3Input;
            set => this.RaiseAndSetIfChanged(ref _textBox3Input, value);
        }
        public string TextBox4Input
        {
            get => _textBox4Input;
            set => this.RaiseAndSetIfChanged(ref _textBox4Input, value);
        }
        public string TextBox5Input
        {
            get => _textBox5Input;
            set => this.RaiseAndSetIfChanged(ref _textBox5Input, value);
        }
        public string LgTextBoxInput
        {
            get => _lgTextBoxInput;
            set => this.RaiseAndSetIfChanged(ref _lgTextBoxInput, value);
        }
        public bool TextBox1Visible
        {
            get => _textBox1Visible;
            set => this.RaiseAndSetIfChanged(ref _textBox1Visible, value);
        }
        public bool TextBox2Visible
        {
            get => _textBox2Visible;
            set => this.RaiseAndSetIfChanged(ref _textBox2Visible, value);
        }
        public bool TextBox3Visible
        {
            get => _textBox3Visible;
            set => this.RaiseAndSetIfChanged(ref _textBox3Visible, value);
        }
        public bool TextBox4Visible
        {
            get => _textBox4Visible;
            set => this.RaiseAndSetIfChanged(ref _textBox4Visible, value);
        }
        public bool TextBox5Visible
        {
            get => _textBox5Visible;
            set => this.RaiseAndSetIfChanged(ref _textBox5Visible, value);
        }
        public bool Date1Visible
        {
            get => _date1Visible;
            set => this.RaiseAndSetIfChanged(ref _date1Visible, value);
        }public bool Date2Visible
        {
            get => _date2Visible;
            set => this.RaiseAndSetIfChanged(ref _date2Visible, value);
        }
        public bool ComboBoxVisible
        {
            get => _comboBoxVisible;
            set => this.RaiseAndSetIfChanged(ref _comboBoxVisible, value);
        }
        public bool CheckBoxVisible
        {
            get => _checkBoxVisible;
            set => this.RaiseAndSetIfChanged(ref _checkBoxVisible, value);
        }
        public bool LgTextBoxVisible
        {
            get => _lgTextBoxVisible;
            set => this.RaiseAndSetIfChanged(ref _lgTextBoxVisible, value);
        }

        public DateTime Date1
        {
            get => _date1;
            set => this.RaiseAndSetIfChanged(ref _date1, value);
        }
        public DateTime Date2
        {
            get => _date2;
            set => this.RaiseAndSetIfChanged(ref _date2, value);
        }

        public bool ChkBox
        {
            get => _chkBox;
            set => this.RaiseAndSetIfChanged(ref _chkBox, value);
        }

        public bool EditEnable
        {
            get => _editEnable;
            set => this.RaiseAndSetIfChanged(ref _editEnable, value);
        }

        public string BorderThickness
        {
            get => _borderThickness;
            set => this.RaiseAndSetIfChanged(ref _borderThickness, value);
        }
        public ReactiveCommand<Window, Unit> CancelBtn { get; }
        public ReactiveCommand<Window, Unit> QueryBtn { get; }

        public QueryViewModel()
        {
            _vaultData = (VaultData) Locator.Current.GetService(typeof(VaultData));
            _vaultPerson = (VaultPerson) Locator.Current.GetService(typeof(VaultPerson));
            QueryBuilder();
            CancelBtn = ReactiveCommand.Create<Window>(CancelClick);
            QueryBtn = ReactiveCommand.Create<Window>(QueryClick);
        }

        private void CancelClick(Window window)
        {
            window.Close();
        }

        private void QueryClick(Window window)
        {
            switch (_type)
            {
                case QueryType.Insert:
                    InsertAction();
                    break;
                case QueryType.Update:
                    UpdateAction();
                    break;
                case QueryType.Delete:
                    DeleteAction();
                    break;
            }
            window.Close();
        }

        private void InsertAction()
        {
            switch (_view)
            {
                case ResumeView.Person:
                    PersonModel person = new() 
                    {
                        FirstName = TextBox1Input, 
                        LastName = TextBox2Input,
                        ContactNumber = TextBox3Input,
                        ContactEmail = TextBox4Input,
                        LinkedIn = TextBox5Input
                    };
                    _vaultData.PersonProcessor.Insert(person);
                    break;
                case ResumeView.Summary:
                    SummaryModel summary = new()
                    {
                        Name = TextBox1Input,
                        Description = LgTextBoxInput,
                        PersonID = _vaultPerson.Person.ID
                    };
                    _vaultData.SummaryProcessor.Insert(summary);
                    break;
                case ResumeView.Address:
                    AddressModel address = new()
                    {
                        Name = TextBox1Input,
                        Address1 = TextBox2Input,
                        Address2 = TextBox3Input,
                        City = TextBox4Input,
                        StateProvence = SelectedItem.Key,
                        Zip = TextBox5Input,
                        PersonID = _vaultPerson.Person.ID
                    };
                    _vaultData.AddressProcessor.Insert(address);
                    break;
                case ResumeView.Education:
                    EducationModel education = new()
                    {
                        SchoolName = TextBox1Input,
                        Major = TextBox2Input,
                        Minor = TextBox3Input,
                        GPA = Convert.ToDecimal(TextBox4Input),
                        StartDate = Date1.Date,
                        EndDate = Date2.Date,
                        Attending = ChkBox,
                        PersonID = _vaultPerson.Person.ID
                    };
                    _vaultData.EducationProcessor.Insert(education);
                    break;
                case ResumeView.Experience:
                    ExperienceModel experience = new()
                    {
                        PersonID = _vaultPerson.Person.ID,
                        Current = ChkBox,
                        Details = LgTextBoxInput,
                        StartDate = Date1,
                        EndDate = Date2,
                        Organization = TextBox1Input,
                        Title = TextBox2Input,
                        StartSalary = Convert.ToDecimal(TextBox3Input),
                        EndSalary = Convert.ToDecimal(TextBox4Input),
                        Category = 1
                    };
                    _vaultData.ExperienceProcessor.Insert(experience);
                    break;
                case ResumeView.Skills:
                    SkillsModel skills = new()
                    {
                        Name = TextBox1Input,
                        StartDate = Date1,
                        Category = SelectedItem.Key,
                        PersonID = _vaultPerson.Person.ID
                    };
                    _vaultData.SkillsProcessor.Insert(skills);
                    break;
                case ResumeView.Misc:
                    MiscModel misc = new()
                    {
                        Name = TextBox1Input,
                        Date = Date1,
                        Description = LgTextBoxInput,
                        Category = SelectedItem.Key,
                        PersonID = _vaultPerson.Person.ID
                    };
                    _vaultData.MiscProcessor.Insert(misc);
                    break;
                case ResumeView.Tag:
                    TagModel tag = new()
                    {
                        Name = TextBox1Input
                    };
                    _vaultData.TagProcessor.Insert(tag);
                    break;
            }
        }

        private void UpdateAction()
        {
            switch (_view)
            {
                case ResumeView.Person:
                    PersonModel personModel = (PersonModel) _model;
                    PersonModel person = new() 
                    {
                        ID = personModel.ID,
                        FirstName = TextBox1Input, 
                        LastName = TextBox2Input,
                        ContactNumber = TextBox3Input,
                        ContactEmail = TextBox4Input,
                        LinkedIn = TextBox5Input
                    };
                    _vaultData.PersonProcessor.Update(person);
                    break;
                case ResumeView.Summary:
                    SummaryModel summaryModel = (SummaryModel) _model;
                    SummaryModel summary = new()
                    {
                        ID = summaryModel.ID,
                        Name = TextBox1Input,
                        Description = LgTextBoxInput,
                        PersonID = _vaultPerson.Person.ID
                    };
                    _vaultData.SummaryProcessor.Update(summary);
                    break;
                case ResumeView.Address:
                    AddressModel addressModel = (AddressModel) _model;
                    AddressModel address = new()
                    {
                        ID = addressModel.ID,
                        Name = TextBox1Input,
                        Address1 = TextBox2Input,
                        Address2 = TextBox3Input,
                        City = TextBox4Input,
                        StateProvence = SelectedItem.Key,
                        Zip = TextBox5Input,
                        PersonID = _vaultPerson.Person.ID
                    };
                    _vaultData.AddressProcessor.Update(address);
                    break;
                case ResumeView.Education:
                    EducationModel educationModel = (EducationModel) _model;
                    EducationModel education = new()
                    {
                        ID = educationModel.ID,
                        SchoolName = TextBox1Input,
                        Major = TextBox2Input,
                        Minor = TextBox3Input,
                        GPA = Convert.ToDecimal(TextBox4Input),
                        StartDate = Date1.Date,
                        EndDate = Date2.Date,
                        Attending = ChkBox,
                        PersonID = _vaultPerson.Person.ID
                    };
                    _vaultData.EducationProcessor.Update(education);
                    break;
                case ResumeView.Experience:
                    ExperienceModel experienceModel = (ExperienceModel) _model;
                    ExperienceModel experience = new()
                    {
                        ID = experienceModel.ID,
                        Category = experienceModel.Category,
                        Current = ChkBox,
                        Details = LgTextBoxInput,
                        StartDate = Date1,
                        EndDate = Date2,
                        Organization = TextBox1Input,
                        Title = TextBox2Input,
                        StartSalary = Convert.ToDecimal(TextBox3Input),
                        EndSalary = Convert.ToDecimal(TextBox4Input),
                        PersonID = _vaultPerson.Person.ID
                    };
                    _vaultData.ExperienceProcessor.Update(experience);
                    break;
                case ResumeView.Skills:
                    SkillsModel skillsModel = (SkillsModel) _model;
                    SkillsModel skills = new()
                    {
                        ID = skillsModel.ID,
                        Name = TextBox1Input,
                        StartDate = Date1,
                        Category = SelectedItem.Key,
                        PersonID = _vaultPerson.Person.ID
                    };
                    _vaultData.SkillsProcessor.Update(skills);
                    break;
                case ResumeView.Misc:
                    MiscModel miscModel = (MiscModel) _model;
                    MiscModel misc = new()
                    {
                        ID = miscModel.ID,
                        Name = TextBox1Input,
                        Date = Date1,
                        Description = LgTextBoxInput,
                        Category = SelectedItem.Key,
                        PersonID = _vaultPerson.Person.ID
                    };
                    _vaultData.MiscProcessor.Update(misc);
                    break;
                case ResumeView.Tag:
                    TagModel tagModel = (TagModel) _model;
                    TagModel tag = new()
                    {
                        ID = tagModel.ID,
                        Name = TextBox1Input
                    };
                    _vaultData.TagProcessor.Update(tag);
                    break;
            }
        }

        private void DeleteAction()
        {
            switch (_view)
            {
                case ResumeView.Person:
                    PersonModel person = (PersonModel) _model;
                    _vaultData.PersonProcessor.Delete(person);
                    break;
                case ResumeView.Summary:
                    SummaryModel summary = (SummaryModel) _model;
                    _vaultData.SummaryProcessor.Delete(summary);
                    break;
                case ResumeView.Address:
                    AddressModel address = (AddressModel) _model;
                    _vaultData.AddressProcessor.Delete(address);
                    break;
                case ResumeView.Education:
                    EducationModel education = (EducationModel) _model;
                    _vaultData.EducationProcessor.Delete(education);
                    break;
                case ResumeView.Experience:
                    ExperienceModel experience = (ExperienceModel) _model;
                    _vaultData.ExperienceProcessor.Delete(experience);
                    break;
                case ResumeView.Skills:
                    SkillsModel skills = (SkillsModel) _model;
                    _vaultData.SkillsProcessor.Delete(skills);
                    break;
                case ResumeView.Misc:
                    MiscModel misc = (MiscModel) _model;
                    _vaultData.MiscProcessor.Delete(misc);
                    break;
                case ResumeView.Tag:
                    TagModel tag = (TagModel) _model;
                    _vaultData.TagProcessor.Delete(tag);
                    break;
            }
        }
        public void QueryBuilder(object model = null!)
        {
            HideAllInput();
            TextBox1Visible = true;
            BorderThickness = "2";
            EditEnable = true;
            switch (_view)
            {
                case ResumeView.Person:
                    SetPerson(model as PersonModel);
                    break;
                case ResumeView.Summary:
                    SetSummary(model as SummaryModel);
                    break;
                case ResumeView.Address:
                    SetAddress(model as AddressModel);
                    break;
                case ResumeView.Education:
                    SetEducation(model as EducationModel);
                    break;
                case ResumeView.Experience:
                    SetExperience(model as ExperienceModel);
                    break;
                case ResumeView.Skills:
                    SetSkills(model as SkillsModel);
                    break;
                case ResumeView.Misc:
                    SetMisc(model as MiscModel);
                    break;
                case ResumeView.Tag:
                    SetTag(model as TagModel);
                    break;
            }
        }

        private void HideAllInput()
        {
            TextBox1Visible = false;
            TextBox2Visible = false;
            TextBox3Visible = false;
            TextBox4Visible = false;
            TextBox5Visible = false;
            Date1Visible = false;
            Date2Visible = false;
            ComboBoxVisible = false;
            CheckBoxVisible = false;
            LgTextBoxVisible = false;
        }

        private void SetWindowHeaders(string tabLabel)
        {
            switch (_type)
            {
                case QueryType.Insert:
                    Header = $"Create New {tabLabel}";
                    QueryButtonLabel = "Add";
                    break;
                case QueryType.Update:
                    Header = $"Edit {tabLabel}";
                    QueryButtonLabel = "Edit";
                    break;
                case QueryType.Delete:
                    Header = $"Delete {tabLabel}";
                    QueryButtonLabel = "Delete";
                    BorderThickness = "0";
                    EditEnable = false;
                    break;
            }
        }

        private void SetPerson(PersonModel? person)
        {
            TextBox2Visible = true;
            TextBox3Visible = true;
            TextBox4Visible = true;
            TextBox5Visible = true;
            Text1Label = "First Name:";
            Text2Label = "Last Name:";
            Text3Label = "Contact #:";
            Text4Label = "Email:";
            Text5Label = "LinkedIn:";
            SetWindowHeaders("Person");
            if (person is not null)
            {
                TextBox1Input = person.FirstName;
                TextBox2Input = person.LastName;
                TextBox3Input = person.ContactNumber;
                TextBox4Input = person.ContactEmail;
                TextBox5Input = person.LinkedIn;
            }

            _model = person!;
        }

        private void SetAddress(AddressModel? address)
        {
            TextBox2Visible = true;
            TextBox3Visible = true;
            TextBox4Visible = true;
            TextBox5Visible = true;
            ComboBoxVisible = true;
            Text1Label = "Name:";
            Text2Label = "Address 1:";
            Text3Label = "Address 2:";
            Text4Label = "City:";
            Text5Label = "Zip:";
            ComboLabel = "State:";
            SetWindowHeaders("Address");
            List<StateProvenceModel> result = _vaultData.StateProvenceProcessor.LoadAllDataList(new StateProvenceModel());
            foreach (var item in result)
            {
                ComboItems.Add(item.ID, item.StateProvence);
            }
            if (address is not null)
            {
                TextBox1Input = address.Name;
                TextBox2Input = address.Address1;
                TextBox3Input = address.Address2;
                TextBox4Input = address.City;
                TextBox5Input = address.Zip;
                SelectedItem = ComboItems.SingleOrDefault(p => p.Key == address.StateProvence);
            }

            _model = address!;
        }

        private void SetEducation(EducationModel? education)
        {
            TextBox2Visible = true;
            TextBox3Visible = true;
            TextBox4Visible = true;
            Date1Visible = true;
            Date2Visible = true;
            CheckBoxVisible = true;
            Text1Label = "School Name:";
            Text2Label = "Major:";
            Text3Label = "Minor:";
            Text4Label = "GPA:";
            Date1Label = "Start Date:";
            Date2Label = "End Date:";
            CheckboxLabel = "Attending:";
            SetWindowHeaders("Education");
            Date1 = DateTime.Today;
            Date2 = DateTime.Today;
            if (education is not null)
            {
                TextBox1Input = education.SchoolName;
                TextBox2Input = education.Major;
                TextBox3Input = education.Minor;
                TextBox4Input = education.GPA.ToString("F2");
                Date1 = education.StartDate;
                Date2 = education.EndDate;
                ChkBox = education.Attending;
            }

            _model = education!;
        }

        private void SetExperience(ExperienceModel? experience)
        {
            TextBox2Visible = true;
            TextBox3Visible = true;
            TextBox4Visible = true;
            Date1Visible = true;
            Date2Visible = true;
            CheckBoxVisible = true;
            LgTextBoxVisible = true;
            Text1Label = "Organization:";
            Text2Label = "Title:";
            Text3Label = "Starting Salary:";
            Text4Label = "Ending Salary:";
            Date1Label = "Start Date:";
            Date2Label = "End Date:";
            CheckboxLabel = "Employed:";
            LgTextLabel = "Details:";
            SetWindowHeaders("Experience");
            Date1 = DateTime.Today;
            Date2 = DateTime.Today;
            TextBox3Input = "$0.00";
            TextBox4Input = "$0.00";
            if (experience is not null)
            {
                TextBox1Input = experience.Organization;
                TextBox2Input = experience.Title;
                TextBox3Input = experience.StartSalary.ToString("C");
                TextBox4Input = experience.EndSalary.ToString("C");
                Date1 = experience.StartDate;
                Date2 = experience.EndDate;
                ChkBox = experience.Current;
                LgTextBoxInput = experience.Details;
            }

            _model = experience!;
        }

        private void SetSkills(SkillsModel? skills)
        {
            Date1Visible = true;
            ComboBoxVisible = true;
            Text1Label = "Name";
            Date1Label = "Start Date:";
            ComboLabel = "Category";
            SetWindowHeaders("Skill");
            Date1 = DateTime.Today;
            List<SkillCategoryModel> result = _vaultData.SkillCategoryProcessor.LoadAllDataList(new SkillCategoryModel());
            foreach (var item in result)
            {
                ComboItems.Add(item.ID, item.Name);
            }
            if (skills is not null)
            {
                TextBox1Input = skills.Name;
                Date1 = skills.StartDate;
                SelectedItem = ComboItems.SingleOrDefault(p => p.Key == skills.Category);
            }

            _model = skills!;
        }

        private void SetMisc(MiscModel? misc)
        {
            Date1Visible = true;
            ComboBoxVisible = true;
            LgTextBoxVisible = true;
            Text1Label = "Name:";
            Date1Label = "Date:";
            ComboLabel = "Category:";
            LgTextLabel = "Description:";
            SetWindowHeaders("Misc");
            Date1 = DateTime.Today;
            List<MiscCategoryModel> result = _vaultData.MiscCategoryProcessor.LoadAllDataList(new MiscCategoryModel());
            foreach (var item in result)
            {
                ComboItems.Add(item.ID, item.Name);
            }

            if (misc is not null)
            {
                TextBox1Input = misc.Name;
                Date1 = misc.Date;
                SelectedItem = ComboItems.SingleOrDefault(p => p.Key == misc.Category);
                LgTextBoxInput = misc.Description;
            }

            _model = misc!;
        }

        private void SetTag(TagModel? tag)
        {
            Text1Label = "Name";
            SetWindowHeaders("Tag");
            if (tag is not null)
            {
                TextBox1Input = tag.Name;
            }

            _model = tag!;
        }

        private void SetSummary(SummaryModel? summary)
        {
            TextBox1Visible = true;
            LgTextBoxVisible = true;
            Text1Label = "Name:";
            LgTextLabel = "Summary:";
            SetWindowHeaders("Summary");
            if (summary is not null)
            {
                TextBox1Input = summary.Name;
                LgTextBoxInput = summary.Description;
            }
            _model = summary!;
        }
    }
}