using Avalonia;
using Avalonia.Controls;
using Avalonia.Markup.Xaml;
using ResumeVaultAvalonia.ViewModels;
using Splat;

namespace ResumeVaultAvalonia.Views
{
    public class TagView : UserControl
    {
        public TagView()
        {
            DataContext = Locator.Current.GetService(typeof(TagViewModel));
            InitializeComponent();
        }

        private void InitializeComponent()
        {
            AvaloniaXamlLoader.Load(this);
        }
    }
}