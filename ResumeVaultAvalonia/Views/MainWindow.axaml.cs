using Avalonia;
using Avalonia.Controls;
using Avalonia.Markup.Xaml;
using ResumeVaultAvalonia.ViewModels;
using Splat;

namespace ResumeVaultAvalonia.Views
{
    public class MainWindow : Window
    {
        public MainWindow()
        {
            DataContext = Locator.Current.GetService(typeof(MainWindowViewModel));
            InitializeComponent();
#if DEBUG
            this.AttachDevTools();
#endif
        }

        private void InitializeComponent()
        {
            AvaloniaXamlLoader.Load(this);
        }
    }
}