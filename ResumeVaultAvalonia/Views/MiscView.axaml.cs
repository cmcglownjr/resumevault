using Avalonia;
using Avalonia.Controls;
using Avalonia.Markup.Xaml;
using ResumeVaultAvalonia.ViewModels;
using Splat;

namespace ResumeVaultAvalonia.Views
{
    public class MiscView : UserControl
    {
        public MiscView()
        {
            DataContext = Locator.Current.GetService(typeof(MiscViewModel));
            InitializeComponent();
        }

        private void InitializeComponent()
        {
            AvaloniaXamlLoader.Load(this);
        }
    }
}