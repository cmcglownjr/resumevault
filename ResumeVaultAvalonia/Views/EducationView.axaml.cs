using Avalonia;
using Avalonia.Controls;
using Avalonia.Markup.Xaml;
using ResumeVaultAvalonia.ViewModels;
using Splat;

namespace ResumeVaultAvalonia.Views
{
    public class EducationView : UserControl
    {
        public EducationView()
        {
            DataContext = Locator.Current.GetService(typeof(EducationViewModel));
            InitializeComponent();
        }

        private void InitializeComponent()
        {
            AvaloniaXamlLoader.Load(this);
        }
    }
}