using Avalonia;
using Avalonia.Controls;
using Avalonia.Markup.Xaml;
using ResumeVaultAvalonia.ViewModels;
using Splat;

namespace ResumeVaultAvalonia.Views
{
    public class SkillsView : UserControl
    {
        public SkillsView()
        {
            DataContext = Locator.Current.GetService(typeof(SkillsViewModel));
            InitializeComponent();
        }

        private void InitializeComponent()
        {
            AvaloniaXamlLoader.Load(this);
        }
    }
}