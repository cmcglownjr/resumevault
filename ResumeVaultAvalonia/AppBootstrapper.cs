using ResumeVaultAvalonia.Models;
using Splat;
using ResumeVaultAvalonia.ViewModels;
using ResumeVaultAvalonia.Views;

namespace ResumeVaultAvalonia
{
    public class AppBootstrapper
    {
        public AppBootstrapper()
        {
            RegisterServices();
        }
        private void RegisterServices()
        {
            Locator.CurrentMutable.RegisterConstant(new VaultData(), typeof(VaultData));
            Locator.CurrentMutable.RegisterConstant(new VaultPerson(), typeof(VaultPerson));
            Locator.CurrentMutable.RegisterConstant(new Models.MessageBox(), typeof(Models.MessageBox));
            Locator.CurrentMutable.RegisterConstant(new SummaryViewModel(), typeof(SummaryViewModel));
            Locator.CurrentMutable.RegisterConstant(new EducationViewModel(), typeof(EducationViewModel));
            Locator.CurrentMutable.RegisterConstant(new ExperienceViewModel(), typeof(ExperienceViewModel));
            Locator.CurrentMutable.RegisterConstant(new AddressViewModel(), typeof(AddressViewModel));
            Locator.CurrentMutable.RegisterConstant(new MiscViewModel(), typeof(MiscViewModel));
            Locator.CurrentMutable.RegisterConstant(new SkillsViewModel(), typeof(SkillsViewModel));
            Locator.CurrentMutable.RegisterConstant(new TagViewModel(), typeof(TagViewModel));
            Locator.CurrentMutable.RegisterConstant(new QueryViewModel(), typeof(QueryViewModel));
            Locator.CurrentMutable.RegisterConstant(new CompareViewModel(), typeof(CompareViewModel));
            Locator.CurrentMutable.RegisterConstant(new PersonViewModel(), typeof(PersonViewModel));
            Locator.CurrentMutable.RegisterConstant(new MainWindowViewModel(), typeof(MainWindowViewModel));
            Locator.CurrentMutable.RegisterConstant(new MainWindow(), typeof(MainWindow));
        }
    }
}