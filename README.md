# Resume Vault

## Introduction

The purpose of this program is to create a SQLite data vault of your professional experiences. Everything that you would add to a resume can be stored here. The inspiration cam to me as I was searching for a job in software engineering and computer programming. As I built up experiences from years of engineering work I sometimes forget important details such as what my duties were at a company or how much I made starting and finishing. The way I would usually keep track is through old resumes. I would infrequently update them, loose them in a cluttered file system,and had no way to really organize the data. There is also online job listing sites like [Indeed](https://www.indeed.com/) but I'm pretty sure they sell your data because I was getting a lot of spam job offers that seemed sketchy.

This is a portfolio project written in C#. The GUI uses the [AvaloniaUI](https://github.com/AvaloniaUI/Avalonia) framework. I used an SQLite database with a plan to expand into using a PostgreSQL database in the future. The features are bare bone and I also hope to expand the basic features it currently has.

### Technologies:

- [.Net](https://dotnet.microsoft.com/download/dotnet-core/3.1) 5.0
- [Avalonia UI](https://avaloniaui.net/) 0.10.6
- [Serilog](https://serilog.net/) 2.10.1
- [Xunit](https://xunit.github.io/) 2.4.1
- [SQLite](https://sqlite.org)
- [Syncfusion](https://www.syncfusion.com/)

### Launch:

In the ResumeVaultAvalonia directory open a terminal and type the following commands:

```bash
dotnet restore
dotnet run
```

### Usage:

This app is pretty straight forward. You create or load a database, add a person (you can have multiple people and keep track of their resumes), and start filling in the blanks of your work history, skills, and accomplishments. There is an option to print your resume to a word document under the edit menu. The word document style and format is present and this currently doesn't allow you to customize the document but you can modify it yourself afterwards.

### Screenshots:

![](Screenshots/person.png)

![](Screenshots/education.png)

![](Screenshots/modify.png)

![](Screenshots/WordDoc.png)

### What I learned?

This was an exercise to improve my understanding of C# syntax, conventions, and also problem solving. I continue to practice Clean Coding styles and unit testing projects. I also learned a bit about using C# to interface with a SQL database. That also lead me to understand how to mock a database for unit testing. There is also what I learned about creating a XAML base user interface using the MVVM pattern.

### What needs work?

There is still a UI bug that sometimes freezes the program after it commits SQL commands. I'm pretty sure I know where its at but I'll hunt it down another time. I want to move on to the next learning exercise. I had a feature where you can add tags to certain resume elements such as experiences and skills. My plan was to use that as a sorting/filtering mechanism, only add to your resume items with this tag sorta thing. I'll sort it out later. I would like to be able to analyze the descriptions you give in your experience to help the user make their resumes more likable. It would suggest improvements to help you stand out more. Too ambitions right now. Finally I'd like to set up reporting mechanisms so you can keep track of how your salary changes over time and how many skills you've gained, when you gained them, and how long you've practiced them. Maybe later.

### License:

This project is licensed under the terms of the MIT License.