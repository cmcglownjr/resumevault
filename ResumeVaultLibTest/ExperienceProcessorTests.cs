using System;
using System.Collections.Generic;
using System.Text;
using Moq;
using Autofac.Extras.Moq;
using ResumeVaultLib.Data.Logic;
using ResumeVaultLib.Data.Models;
using ResumeVaultLib.Data.Utilities;
using Xunit;

namespace ResumeVaultLibTest
{
    public class ExperienceProcessorTests
    {
        private ExperienceModel _experience = new()
        {
            ID = 1,
            PersonID = 1,
            Organization = "Company L.L.C.",
            Title = "Engineer",
            Details = "I did stuff!",
            StartDate = new DateTime(2010, 01, 01),
            EndDate = new DateTime(2020, 12, 31),
            StartSalary = 50000M,
            EndSalary = 85000M,
            Current = false,
            Category = 1
        };

        [Fact]
        public void Insert_ValidCall()
        {
            string sql = @"insert into Experience (PersonID, Organization, Title, Details, StartDate, EndDate, Current, 
                        StartSalary, EndSalary, Category) VALUES (@PersonID, @Organization, @Title, @Details, 
                                                                  @StartDate, @EndDate, @Current, @StartSalary, 
                                                                  @EndSalary, @Category)";
            using (var mock = AutoMock.GetLoose())
            {
                mock.Mock<IDatabaseAccess>()
                    .Setup(x => x.InsertQuery(_experience, sql));
                var cls = mock.Create<ExperienceProcessor>();
                cls.Insert(_experience);
                mock.Mock<IDatabaseAccess>()
                    .Verify(x => x.InsertQuery(_experience, sql), Times.Exactly(1));
            }
        }

        [Fact]
        public void Update_ValidCall()
        {
            string sql = @"update Experience set PersonID=@PersonID, Organization=@Organization, Title=@Title, 
                      Details=@Details, StartDate=@StartDate, EndDate=@EndDate, Current=@Current, 
                      StartSalary=@StartSalary, EndSalary=@EndSalary, Category=@Category where ID=@ID";
            using (var mock = AutoMock.GetLoose())
            {
                mock.Mock<IDatabaseAccess>()
                    .Setup(x => x.UpdateQuery(_experience, sql));
                var cls = mock.Create<ExperienceProcessor>();
                cls.Update(_experience);
                mock.Mock<IDatabaseAccess>()
                    .Verify(x => x.UpdateQuery(_experience, sql), Times.Exactly(1));
            }
        }

        [Fact]
        public void Delete_ValidCall()
        {
            string sql = "delete from Experience where ID=@ID";
            using (var mock = AutoMock.GetLoose())
            {
                mock.Mock<IDatabaseAccess>()
                    .Setup(x => x.DeleteQuery(_experience, sql));
                var cls = mock.Create<ExperienceProcessor>();
                cls.Delete(_experience);
                mock.Mock<IDatabaseAccess>()
                    .Verify(x => x.DeleteQuery(_experience, sql), Times.Exactly(1));
            }
        }

        [Fact]
        public void LoadDataSingle_GetExperienceModel()
        {
            string sql = "select * from Experience where ID=@ID";
            using (var mock = AutoMock.GetLoose())
            {
                mock.Mock<IDatabaseAccess>()
                    .Setup(x => x.SelectQuerySingle(_experience, sql))
                    .Returns(_experience);
                var cls = mock.Create<ExperienceProcessor>();
                var expected = _experience;
                var actual = cls.LoadDataSingle(_experience);
                Assert.True(actual != null);
                Assert.Equal(expected.ID, actual.ID);
            }
        }
        [Fact]
        public void LoadDataList_ValidCall()
        {
            string sql = "select * from Experience";
            using (var mock = AutoMock.GetLoose())
            {
                mock.Mock<IDatabaseAccess>()
                    .Setup(x => x.SelectQueryList(_experience, sql))
                    .Returns(GetSampleData());
                var cls = mock.Create<ExperienceProcessor>();
                var expected = GetSampleData();
                var actual = cls.LoadAllDataList(_experience);
                Assert.True(actual != null);
                Assert.Equal(expected.Count, actual.Count);
            }
        }
        
        [Fact]
        public void LoadDataListByPerson_ValidCall()
        {
            string sql = "select * from Experience where PersonID=@PersonID";
            using (var mock = AutoMock.GetLoose())
            {
                mock.Mock<IDatabaseAccess>()
                    .Setup(x => x.SelectQueryList(_experience, sql))
                    .Returns(GetSampleData());
                var cls = mock.Create<ExperienceProcessor>();
                var expected = GetSampleData();
                var actual = cls.LoadDataListByPerson(_experience);
                Assert.True(actual != null);
                Assert.Equal(expected.Count, actual.Count);
            }
        }

        [Fact]
        public void LoadSelectDataList_validCall()
        {
            List<int> idList = new() {1, 2, 3};
            StringBuilder builder = new StringBuilder();
            foreach (var item in idList)
            {
                if (idList.IndexOf(item) == idList.Count - 1)
                {
                    builder.Append(item);
                }
                else
                {
                    builder.Append(item).Append(", ");
                }
            }
            var ids = builder.ToString();
            string sql = $"select * from Experience where ID in ({ids}) ORDER BY StartDate DESC";
            using (var mock = AutoMock.GetLoose())
            {
                mock.Mock<IDatabaseAccess>()
                    .Setup(x => x.SelectQueryList(_experience, sql))
                    .Returns(GetSampleData());
                var cls = mock.Create<ExperienceProcessor>();
                var expected = GetSampleData();
                var actual = cls.LoadSelectDataList(_experience, idList);
                Assert.True(actual != null);
                Assert.Equal(expected.Count, actual.Count);
            }
        }

        private List<ExperienceModel> GetSampleData()
        {
            List<ExperienceModel> output = new()
            {
                new ExperienceModel()
                {
                    ID = 1
                },
                new ExperienceModel()
                {
                    ID = 2
                },
                new ExperienceModel()
                {
                    ID = 3
                }
            };
            return output;
        }
    }
}