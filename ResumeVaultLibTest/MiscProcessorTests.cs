using System.Collections.Generic;
using System.Text;
using Moq;
using Autofac.Extras.Moq;
using ResumeVaultLib.Data.Logic;
using ResumeVaultLib.Data.Models;
using ResumeVaultLib.Data.Utilities;
using Xunit;

namespace ResumeVaultLibTest
{
    public class MiscProcessorTests
    {
        private MiscModel _misc = new()
        {
            ID = 1
        };

        [Fact]
        public void Insert_ValidCall()
        {
            string sql =  @"insert into Misc (PersonID, Name, Description, Date, Category) values 
                        (@PersonID, @Name, @Description, @Date, @Category)";
            using (var mock = AutoMock.GetLoose())
            {
                mock.Mock<IDatabaseAccess>()
                    .Setup(x => x.InsertQuery(_misc, sql));
                var cls = mock.Create<MiscProcessor>();
                cls.Insert(_misc);
                mock.Mock<IDatabaseAccess>()
                    .Verify(x => x.InsertQuery(_misc, sql), Times.Exactly(1));
            }
        }

        [Fact]
        public void Update_ValidCall()
        {
            string sql = @"update Misc set PersonID=@PersonID, Name=@Name, Description=@Description, Date=@Date, 
                        Category=@Category where ID=@ID";
            using (var mock = AutoMock.GetLoose())
            {
                mock.Mock<IDatabaseAccess>()
                    .Setup(x => x.UpdateQuery(_misc, sql));
                var cls = mock.Create<MiscProcessor>();
                cls.Update(_misc);
                mock.Mock<IDatabaseAccess>()
                    .Verify(x => x.UpdateQuery(_misc, sql), Times.Exactly(1));
            }
        }

        [Fact]
        public void Delete_ValidCall()
        {
            string sql = "delete from Misc where ID=@ID";
            using (var mock = AutoMock.GetLoose())
            {
                mock.Mock<IDatabaseAccess>()
                    .Setup(x => x.DeleteQuery(_misc, sql));
                var cls = mock.Create<MiscProcessor>();
                cls.Delete(_misc);
                mock.Mock<IDatabaseAccess>()
                    .Verify(x => x.DeleteQuery(_misc, sql), Times.Exactly(1));
            }
        }

        [Fact]
        public void LoadDataSingle_GetExperienceModel()
        {
            string sql = "select * from Misc where ID=@ID";
            using (var mock = AutoMock.GetLoose())
            {
                mock.Mock<IDatabaseAccess>()
                    .Setup(x => x.SelectQuerySingle(_misc, sql))
                    .Returns(_misc);
                var cls = mock.Create<MiscProcessor>();
                var expected = _misc;
                var actual = cls.LoadDataSingle(_misc);
                Assert.True(actual != null);
                Assert.Equal(expected.ID, actual.ID);
            }
        }
        [Fact]
        public void LoadDataList_ValidCall()
        {
            string sql = "select * from Misc";
            using (var mock = AutoMock.GetLoose())
            {
                mock.Mock<IDatabaseAccess>()
                    .Setup(x => x.SelectQueryList(_misc, sql))
                    .Returns(GetSampleData());
                var cls = mock.Create<MiscProcessor>();
                var expected = GetSampleData();
                var actual = cls.LoadAllDataList(_misc);
                Assert.True(actual != null);
                Assert.Equal(expected.Count, actual.Count);
            }
        }
        
        [Fact]
        public void LoadDataListByPerson_ValidCall()
        {
            string sql = @"select * from Misc where PersonID=@PersonID";
            using (var mock = AutoMock.GetLoose())
            {
                mock.Mock<IDatabaseAccess>()
                    .Setup(x => x.SelectQueryList(_misc, sql))
                    .Returns(GetSampleData());
                var cls = mock.Create<MiscProcessor>();
                var expected = GetSampleData();
                var actual = cls.LoadDataListByPerson(_misc);
                Assert.True(actual != null);
                Assert.Equal(expected.Count, actual.Count);
            }
        }
        [Fact]
        public void LoadSelectDataList_validCall()
        {
            List<int> idList = new() {1, 2, 3};
            StringBuilder builder = new StringBuilder();
            foreach (var item in idList)
            {
                if (idList.IndexOf(item) == idList.Count - 1)
                {
                    builder.Append(item);
                }
                else
                {
                    builder.Append(item).Append(", ");
                }
            }
            var ids = builder.ToString();
            string sql = $"select * from Misc where ID in ({ids});";
            using (var mock = AutoMock.GetLoose())
            {
                mock.Mock<IDatabaseAccess>()
                    .Setup(x => x.SelectQueryList(_misc, sql))
                    .Returns(GetSampleData());
                var cls = mock.Create<MiscProcessor>();
                var expected = GetSampleData();
                var actual = cls.LoadSelectDataList(_misc, idList);
                Assert.True(actual != null);
                Assert.Equal(expected.Count, actual.Count);
            }
        }

        private List<MiscModel> GetSampleData()
        {
            List<MiscModel> output = new()
            {
                new MiscModel()
                {
                    ID = 1
                },
                new MiscModel()
                {
                    ID = 2
                },
                new MiscModel()
                {
                    ID = 3
                }
            };
            return output;
        }
    }
}