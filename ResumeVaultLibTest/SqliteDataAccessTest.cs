using System;
using System.IO;
using System.Reflection;
using Moq;
using Autofac.Extras.Moq;
using ResumeVaultLib.Data.Utilities;
using Xunit;

namespace ResumeVaultLibTest
{
    public class SqliteDataAccessTest
    {
        string _homeFolder = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile);
        
        
       [Fact]
        public void CreateDatabase_Test_EmptyStrings()
        {
            string expectedString = Path.Combine(_homeFolder, "ResumeVault.db");
            FileInfo expected = new FileInfo(expectedString);
            IDatabaseAccess database = new SqliteDataAccess();
            FileInfo actual = database.CreateDatabase("", "");
            Assert.Equal(expected.FullName, actual.FullName);
            actual.Delete();
        }
    }
}