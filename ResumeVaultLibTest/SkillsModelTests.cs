using System.Collections.Generic;
using System.Text;
using Moq;
using Autofac.Extras.Moq;
using ResumeVaultLib.Data.Logic;
using ResumeVaultLib.Data.Models;
using ResumeVaultLib.Data.Utilities;
using Xunit;

namespace ResumeVaultLibTest
{
    public class SkillsModelTests
    {
        private SkillsModel _skills = new()
        {
            ID = 1
        };

        [Fact]
        public void Insert_ValidCall()
        {
            string sql =  @"insert into Skills (Name, StartDate, PersonID, Category) values 
                                                                (@Name, @StartDate, @PersonID, @Category)";
            using (var mock = AutoMock.GetLoose())
            {
                mock.Mock<IDatabaseAccess>()
                    .Setup(x => x.InsertQuery(_skills, sql));
                var cls = mock.Create<SkillsProcessor>();
                cls.Insert(_skills);
                mock.Mock<IDatabaseAccess>()
                    .Verify(x => x.InsertQuery(_skills, sql), Times.Exactly(1));
            }
        }

        [Fact]
        public void Update_ValidCall()
        {
            string sql = @"update Skills set Name=@Name, StartDate=@StartDate, PersonID=@PersonID, Category=@Category 
                            where ID=@ID";
            using (var mock = AutoMock.GetLoose())
            {
                mock.Mock<IDatabaseAccess>()
                    .Setup(x => x.UpdateQuery(_skills, sql));
                var cls = mock.Create<SkillsProcessor>();
                cls.Update(_skills);
                mock.Mock<IDatabaseAccess>()
                    .Verify(x => x.UpdateQuery(_skills, sql), Times.Exactly(1));
            }
        }

        [Fact]
        public void Delete_ValidCall()
        {
            string sql = "delete from Skills where ID=@ID";
            using (var mock = AutoMock.GetLoose())
            {
                mock.Mock<IDatabaseAccess>()
                    .Setup(x => x.DeleteQuery(_skills, sql));
                var cls = mock.Create<SkillsProcessor>();
                cls.Delete(_skills);
                mock.Mock<IDatabaseAccess>()
                    .Verify(x => x.DeleteQuery(_skills, sql), Times.Exactly(1));
            }
        }

        [Fact]
        public void LoadDataSingle_GetExperienceModel()
        {
            string sql = "select * from Skills where ID=@ID";
            using (var mock = AutoMock.GetLoose())
            {
                mock.Mock<IDatabaseAccess>()
                    .Setup(x => x.SelectQuerySingle(_skills, sql))
                    .Returns(_skills);
                var cls = mock.Create<SkillsProcessor>();
                var expected = _skills;
                var actual = cls.LoadDataSingle(_skills);
                Assert.True(actual != null);
                Assert.Equal(expected.ID, actual.ID);
            }
        }
        [Fact]
        public void LoadDataList_ValidCall()
        {
            string sql = "select * from Skills";
            using (var mock = AutoMock.GetLoose())
            {
                mock.Mock<IDatabaseAccess>()
                    .Setup(x => x.SelectQueryList(_skills, sql))
                    .Returns(GetSampleData());
                var cls = mock.Create<SkillsProcessor>();
                var expected = GetSampleData();
                var actual = cls.LoadAllDataList(_skills);
                Assert.True(actual != null);
                Assert.Equal(expected.Count, actual.Count);
            }
        }
        
        [Fact]
        public void LoadDataListByPerson_ValidCall()
        {
            string sql = @"select * from Skills where PersonID=@PersonID";
            using (var mock = AutoMock.GetLoose())
            {
                mock.Mock<IDatabaseAccess>()
                    .Setup(x => x.SelectQueryList(_skills, sql))
                    .Returns(GetSampleData());
                var cls = mock.Create<SkillsProcessor>();
                var expected = GetSampleData();
                var actual = cls.LoadDataListByPerson(_skills);
                Assert.True(actual != null);
                Assert.Equal(expected.Count, actual.Count);
            }
        }
        [Fact]
        public void LoadSelectDataList_validCall()
        {
            List<int> idList = new() {1, 2, 3};
            StringBuilder builder = new StringBuilder();
            foreach (var item in idList)
            {
                if (idList.IndexOf(item) == idList.Count - 1)
                {
                    builder.Append(item);
                }
                else
                {
                    builder.Append(item).Append(", ");
                }
            }
            var ids = builder.ToString();
            string sql = $"select * from Skills where ID in ({ids});";
            using (var mock = AutoMock.GetLoose())
            {
                mock.Mock<IDatabaseAccess>()
                    .Setup(x => x.SelectQueryList(_skills, sql))
                    .Returns(GetSampleData());
                var cls = mock.Create<SkillsProcessor>();
                var expected = GetSampleData();
                var actual = cls.LoadSelectDataList(_skills, idList);
                Assert.True(actual != null);
                Assert.Equal(expected.Count, actual.Count);
            }
        }

        private List<SkillsModel> GetSampleData()
        {
            List<SkillsModel> output = new()
            {
                new SkillsModel()
                {
                    ID = 1
                },
                new SkillsModel()
                {
                    ID = 2
                },
                new SkillsModel()
                {
                    ID = 3
                }
            };
            return output;
        }
    }
}