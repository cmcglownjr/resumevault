using System.Collections.Generic;
using Moq;
using Autofac.Extras.Moq;
using ResumeVaultLib.Data.Logic;
using ResumeVaultLib.Data.Models;
using ResumeVaultLib.Data.Utilities;
using Xunit;

namespace ResumeVaultLibTest
{
    public class TagSkillBridgeProcessorTests
    {
        private TagSkillBridgeModel _tagSkill = new()
        {
            ID = 1
        };

        [Fact]
        public void Insert_ValidCall()
        {
            string sql = "insert into TagSkillBridge (SkillID, TagID) values (@SkillID, @TagID)";
            using (var mock = AutoMock.GetLoose())
            {
                mock.Mock<IDatabaseAccess>()
                    .Setup(x => x.InsertQuery(_tagSkill, sql));
                var cls = mock.Create<TagSkillBridgeProcessor>();
                cls.Insert(_tagSkill);
                mock.Mock<IDatabaseAccess>()
                    .Verify(x => x.InsertQuery(_tagSkill, sql), Times.Exactly(1));
            }
        }

        [Fact]
        public void Update_ValidCall()
        {
            string sql = "update TagSkillBridge set SkillID=@SkillID, TagID=@TagID where ID=@ID";
            using (var mock = AutoMock.GetLoose())
            {
                mock.Mock<IDatabaseAccess>()
                    .Setup(x => x.UpdateQuery(_tagSkill, sql));
                var cls = mock.Create<TagSkillBridgeProcessor>();
                cls.Update(_tagSkill);
                mock.Mock<IDatabaseAccess>()
                    .Verify(x => x.UpdateQuery(_tagSkill, sql), Times.Exactly(1));
            }
        }

        [Fact]
        public void Delete_ValidCall()
        {
            string sql = "delete from TagSkillBridge where ID=@ID";
            using (var mock = AutoMock.GetLoose())
            {
                mock.Mock<IDatabaseAccess>()
                    .Setup(x => x.DeleteQuery(_tagSkill, sql));
                var cls = mock.Create<TagSkillBridgeProcessor>();
                cls.Delete(_tagSkill);
                mock.Mock<IDatabaseAccess>()
                    .Verify(x => x.DeleteQuery(_tagSkill, sql), Times.Exactly(1));
            }
        }

        [Fact]
        public void LoadDataSingle_GetExperienceModel()
        {
            string sql = "select * from TagSkillBridge where ID=@ID";
            using (var mock = AutoMock.GetLoose())
            {
                mock.Mock<IDatabaseAccess>()
                    .Setup(x => x.SelectQuerySingle(_tagSkill, sql))
                    .Returns(_tagSkill);
                var cls = mock.Create<TagSkillBridgeProcessor>();
                var expected = _tagSkill;
                var actual = cls.LoadDataSingle(_tagSkill);
                Assert.True(actual != null);
                Assert.Equal(expected.ID, actual.ID);
            }
        }
        [Fact]
        public void LoadDataList_ValidCall()
        {
            string sql = "select * from TagSkillBridge";
            using (var mock = AutoMock.GetLoose())
            {
                mock.Mock<IDatabaseAccess>()
                    .Setup(x => x.SelectQueryList(_tagSkill, sql))
                    .Returns(GetSampleData());
                var cls = mock.Create<TagSkillBridgeProcessor>();
                var expected = GetSampleData();
                var actual = cls.LoadAllDataList(_tagSkill);
                Assert.True(actual != null);
                Assert.Equal(expected.Count, actual.Count);
            }
        }

        private List<TagSkillBridgeModel> GetSampleData()
        {
            List<TagSkillBridgeModel> output = new()
            {
                new TagSkillBridgeModel()
                {
                    ID = 1
                },
                new TagSkillBridgeModel()
                {
                    ID = 2
                },
                new TagSkillBridgeModel()
                {
                    ID = 3
                }
            };
            return output;
        }
    }
}