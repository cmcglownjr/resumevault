using System.Collections.Generic;
using Moq;
using Autofac.Extras.Moq;
using ResumeVaultLib.Data.Logic;
using ResumeVaultLib.Data.Models;
using ResumeVaultLib.Data.Utilities;
using Xunit;

namespace ResumeVaultLibTest
{
    public class StateProvenceProcessorTests
    {
        private StateProvenceModel _stateProvence = new()
        {
            ID = 1
        };

        [Fact]
        public void Insert_ValidCall()
        {
            string sql = "insert into StateProvence (StateProvence, Country) values (@StateProvence, @Country)";
            using (var mock = AutoMock.GetLoose())
            {
                mock.Mock<IDatabaseAccess>()
                    .Setup(x => x.InsertQuery(_stateProvence, sql));
                var cls = mock.Create<StateProvenceProcessor>();
                cls.Insert(_stateProvence);
                mock.Mock<IDatabaseAccess>()
                    .Verify(x => x.InsertQuery(_stateProvence, sql), Times.Exactly(1));
            }
        }

        [Fact]
        public void Update_ValidCall()
        {
            string sql = "update StateProvence set StateProvence=@StateProvence, Country=@Country where ID+@ID";
            using (var mock = AutoMock.GetLoose())
            {
                mock.Mock<IDatabaseAccess>()
                    .Setup(x => x.UpdateQuery(_stateProvence, sql));
                var cls = mock.Create<StateProvenceProcessor>();
                cls.Update(_stateProvence);
                mock.Mock<IDatabaseAccess>()
                    .Verify(x => x.UpdateQuery(_stateProvence, sql), Times.Exactly(1));
            }
        }

        [Fact]
        public void Delete_ValidCall()
        {
            string sql = "delete from StateProvence where ID=@ID";
            using (var mock = AutoMock.GetLoose())
            {
                mock.Mock<IDatabaseAccess>()
                    .Setup(x => x.DeleteQuery(_stateProvence, sql));
                var cls = mock.Create<StateProvenceProcessor>();
                cls.Delete(_stateProvence);
                mock.Mock<IDatabaseAccess>()
                    .Verify(x => x.DeleteQuery(_stateProvence, sql), Times.Exactly(1));
            }
        }

        [Fact]
        public void LoadDataSingle_GetExperienceModel()
        {
            string sql = "select * from StateProvence where ID=@ID";
            using (var mock = AutoMock.GetLoose())
            {
                mock.Mock<IDatabaseAccess>()
                    .Setup(x => x.SelectQuerySingle(_stateProvence, sql))
                    .Returns(_stateProvence);
                var cls = mock.Create<StateProvenceProcessor>();
                var expected = _stateProvence;
                var actual = cls.LoadDataSingle(_stateProvence);
                Assert.True(actual != null);
                Assert.Equal(expected.ID, actual.ID);
            }
        }
        [Fact]
        public void LoadDataList_ValidCall()
        {
            string sql = "select * from StateProvence";
            using (var mock = AutoMock.GetLoose())
            {
                mock.Mock<IDatabaseAccess>()
                    .Setup(x => x.SelectQueryList(_stateProvence, sql))
                    .Returns(GetSampleData());
                var cls = mock.Create<StateProvenceProcessor>();
                var expected = GetSampleData();
                var actual = cls.LoadAllDataList(_stateProvence);
                Assert.True(actual != null);
                Assert.Equal(expected.Count, actual.Count);
            }
        }

        private List<StateProvenceModel> GetSampleData()
        {
            List<StateProvenceModel> output = new()
            {
                new StateProvenceModel()
                {
                    ID = 1
                },
                new StateProvenceModel()
                {
                    ID = 2
                },
                new StateProvenceModel()
                {
                    ID = 3
                }
            };
            return output;
        }
    }
}