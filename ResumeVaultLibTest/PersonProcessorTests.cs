using System.Collections.Generic;
using Moq;
using Autofac.Extras.Moq;
using ResumeVaultLib.Data.Logic;
using ResumeVaultLib.Data.Models;
using ResumeVaultLib.Data.Utilities;
using Xunit;

namespace ResumeVaultLibTest
{
    public class PersonProcessorTests
    {
        private PersonModel _person = new()
        {
            ID = 1,
            FirstName = "Jon",
            LastName = "Doe"
        };
        [Fact]
        public void Insert_ValidCall()
        {
            string sql = @"insert into Person (FirstName, LastName, ContactNumber, ContactEmail, LinkedIn) Values 
            (@FirstName, @LastName, @ContactNumber, @ContactEmail, @LinkedIn)";
            using (var mock = AutoMock.GetLoose())
            {
                mock.Mock<IDatabaseAccess>()
                    .Setup(x => x.InsertQuery(_person, sql));
                var cls = mock.Create<PersonProcessor>();
                cls.Insert(_person);
                mock.Mock<IDatabaseAccess>()
                    .Verify(x => x.InsertQuery(_person, sql), Times.Exactly(1));
            }
        }

        [Fact]
        public void Update_ValidCall()
        {
            string sql = @"update Person set FirstName=@FirstName, LastName=@LastName, ContactNumber=@ContactNumber, 
            ContactEmail=@ContactEmail, LinkedIn=@LinkedIn where ID=@ID";
            using (var mock = AutoMock.GetLoose())
            {
                mock.Mock<IDatabaseAccess>()
                    .Setup(x => x.UpdateQuery(_person, sql));
                var cls = mock.Create<PersonProcessor>();
                cls.Update(_person);
                mock.Mock<IDatabaseAccess>()
                    .Verify(x => x.UpdateQuery(_person, sql), Times.Exactly(1));
            }
        }

        [Fact]
        public void Delete_ValidCall()
        {
            string sql = "delete from Person where ID=@ID";
            using (var mock = AutoMock.GetLoose())
            {
                mock.Mock<IDatabaseAccess>()
                    .Setup(x => x.DeleteQuery(_person, sql));
                var cls = mock.Create<PersonProcessor>();
                cls.Delete(_person);
                mock.Mock<IDatabaseAccess>()
                    .Verify(x => x.DeleteQuery(_person, sql), Times.Exactly(1));
            }
        }
        [Fact]
        public void LoadDataSingle_GetPersonModel()
        {
            string sql = "select * from Person where ID=@ID";
            using (var mock = AutoMock.GetLoose())
            {
                mock.Mock<IDatabaseAccess>()
                    .Setup(x => x.SelectQuerySingle(_person, sql))
                    .Returns(_person);
                var cls = mock.Create<PersonProcessor>();
                var expected = _person;
                var actual = cls.LoadDataSingle(_person);
                Assert.True(actual != null);
                Assert.Equal(expected.ID, actual.ID);
            }
        }

        [Fact]
        public void LoadDataList_ValidCall()
        {
            string sql = "select * from Person";
            using (var mock = AutoMock.GetLoose())
            {
                mock.Mock<IDatabaseAccess>()
                    .Setup(x => x.SelectQueryList<PersonModel>(_person, sql))
                    .Returns(GetSampleData());
                var cls = mock.Create<PersonProcessor>();
                var expected = GetSampleData();
                var actual = cls.LoadAllDataList(_person);
                Assert.True(actual != null);
                Assert.Equal(expected.Count, actual.Count);
            }
        }

        private List<PersonModel> GetSampleData()
        {
            List<PersonModel> output = new()
            {
                new PersonModel()
                {
                    ID = 1,
                    FirstName = "Jon",
                    LastName = "Doe"
                },
                new PersonModel()
                {
                    ID = 2,
                    FirstName = "Jane",
                    LastName = "Doe"
                },
                new PersonModel()
                {
                    ID = 3,
                    FirstName = "Roger",
                    LastName = "Smith"
                }
            };
            return output;
        }
    }
}