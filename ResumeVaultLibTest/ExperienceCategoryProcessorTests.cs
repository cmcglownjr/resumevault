using System.Collections.Generic;
using Moq;
using Autofac.Extras.Moq;
using ResumeVaultLib.Data.Logic;
using ResumeVaultLib.Data.Models;
using ResumeVaultLib.Data.Utilities;
using Xunit;

namespace ResumeVaultLibTest
{
    public class ExperienceCategoryProcessorTests
    {
        private ExperienceCategoryModel _experienceCategory = new()
        {
            ID = 1
        };

        [Fact]
        public void Insert_ValidCall()
        {
            string sql =  "insert into ExperienceCategory (Name) values (@Name)";
            using (var mock = AutoMock.GetLoose())
            {
                mock.Mock<IDatabaseAccess>()
                    .Setup(x => x.InsertQuery(_experienceCategory, sql));
                var cls = mock.Create<ExperienceCategoryProcessor>();
                cls.Insert(_experienceCategory);
                mock.Mock<IDatabaseAccess>()
                    .Verify(x => x.InsertQuery(_experienceCategory, sql), Times.Exactly(1));
            }
        }

        [Fact]
        public void Update_ValidCall()
        {
            string sql = "update ExperienceCategory set Name=@Name where ID=@ID";
            using (var mock = AutoMock.GetLoose())
            {
                mock.Mock<IDatabaseAccess>()
                    .Setup(x => x.UpdateQuery(_experienceCategory, sql));
                var cls = mock.Create<ExperienceCategoryProcessor>();
                cls.Update(_experienceCategory);
                mock.Mock<IDatabaseAccess>()
                    .Verify(x => x.UpdateQuery(_experienceCategory, sql), Times.Exactly(1));
            }
        }

        [Fact]
        public void Delete_ValidCall()
        {
            string sql = "delete from ExperienceCategory where ID=@ID";
            using (var mock = AutoMock.GetLoose())
            {
                mock.Mock<IDatabaseAccess>()
                    .Setup(x => x.DeleteQuery(_experienceCategory, sql));
                var cls = mock.Create<ExperienceCategoryProcessor>();
                cls.Delete(_experienceCategory);
                mock.Mock<IDatabaseAccess>()
                    .Verify(x => x.DeleteQuery(_experienceCategory, sql), Times.Exactly(1));
            }
        }

        [Fact]
        public void LoadDataSingle_GetExperienceModel()
        {
            string sql = "select * from ExperienceCategory where ID=@ID";
            using (var mock = AutoMock.GetLoose())
            {
                mock.Mock<IDatabaseAccess>()
                    .Setup(x => x.SelectQuerySingle(_experienceCategory, sql))
                    .Returns(_experienceCategory);
                var cls = mock.Create<ExperienceCategoryProcessor>();
                var expected = _experienceCategory;
                var actual = cls.LoadDataSingle(_experienceCategory);
                Assert.True(actual != null);
                Assert.Equal(expected.ID, actual.ID);
            }
        }
        [Fact]
        public void LoadDataList_ValidCall()
        {
            string sql = "select * from ExperienceCategory";
            using (var mock = AutoMock.GetLoose())
            {
                mock.Mock<IDatabaseAccess>()
                    .Setup(x => x.SelectQueryList(_experienceCategory, sql))
                    .Returns(GetSampleData());
                var cls = mock.Create<ExperienceCategoryProcessor>();
                var expected = GetSampleData();
                var actual = cls.LoadAllDataList(_experienceCategory);
                Assert.True(actual != null);
                Assert.Equal(expected.Count, actual.Count);
            }
        }

        private List<ExperienceCategoryModel> GetSampleData()
        {
            List<ExperienceCategoryModel> output = new()
            {
                new ExperienceCategoryModel()
                {
                    ID = 1
                },
                new ExperienceCategoryModel()
                {
                    ID = 2
                },
                new ExperienceCategoryModel()
                {
                    ID = 3
                }
            };
            return output;
        }
    }
}