using System.Collections.Generic;
using Moq;
using Autofac.Extras.Moq;
using ResumeVaultLib.Data.Logic;
using ResumeVaultLib.Data.Models;
using ResumeVaultLib.Data.Utilities;
using Xunit;

namespace ResumeVaultLibTest
{
    public class SummaryProcessorTests
    {
        private SummaryModel _summary = new()
        {
            ID = 1
        };

        [Fact]
        public void Insert_ValidCall()
        {
            string sql = "insert into Summary (PersonID, Name, Description) values (@PersonID, @Name, @Description)";
            using (var mock = AutoMock.GetLoose())
            {
                mock.Mock<IDatabaseAccess>()
                    .Setup(x => x.InsertQuery(_summary, sql));
                var cls = mock.Create<SummaryProcessor>();
                cls.Insert(_summary);
                mock.Mock<IDatabaseAccess>()
                    .Verify(x => x.InsertQuery(_summary, sql), Times.Exactly(1));
            }
        }

        [Fact]
        public void Update_ValidCall()
        {
            string sql = "update Summary set PersonID=@PersonID, Name=@Name, Description=@Description where ID=@ID";
            using (var mock = AutoMock.GetLoose())
            {
                mock.Mock<IDatabaseAccess>()
                    .Setup(x => x.UpdateQuery(_summary, sql));
                var cls = mock.Create<SummaryProcessor>();
                cls.Update(_summary);
                mock.Mock<IDatabaseAccess>()
                    .Verify(x => x.UpdateQuery(_summary, sql), Times.Exactly(1));
            }
        }

        [Fact]
        public void Delete_ValidCall()
        {
            string sql = "delete from Summary where ID=@ID";
            using (var mock = AutoMock.GetLoose())
            {
                mock.Mock<IDatabaseAccess>()
                    .Setup(x => x.DeleteQuery(_summary, sql));
                var cls = mock.Create<SummaryProcessor>();
                cls.Delete(_summary);
                mock.Mock<IDatabaseAccess>()
                    .Verify(x => x.DeleteQuery(_summary, sql), Times.Exactly(1));
            }
        }

        [Fact]
        public void LoadDataSingle_GetExperienceModel()
        {
            string sql = "select * from Summary where ID=@ID";
            using (var mock = AutoMock.GetLoose())
            {
                mock.Mock<IDatabaseAccess>()
                    .Setup(x => x.SelectQuerySingle(_summary, sql))
                    .Returns(_summary);
                var cls = mock.Create<SummaryProcessor>();
                var expected = _summary;
                var actual = cls.LoadDataSingle(_summary);
                Assert.True(actual != null);
                Assert.Equal(expected.ID, actual.ID);
            }
        }
        [Fact]
        public void LoadDataList_ValidCall()
        {
            string sql = "select * from Summary";
            using (var mock = AutoMock.GetLoose())
            {
                mock.Mock<IDatabaseAccess>()
                    .Setup(x => x.SelectQueryList(_summary, sql))
                    .Returns(GetSampleData());
                var cls = mock.Create<SummaryProcessor>();
                var expected = GetSampleData();
                var actual = cls.LoadAllDataList(_summary);
                Assert.True(actual != null);
                Assert.Equal(expected.Count, actual.Count);
            }
        }
        
        [Fact]
        public void LoadDataListByPerson_ValidCall()
        {
            string sql = @"select * from Summary where PersonID=@PersonID";
            using (var mock = AutoMock.GetLoose())
            {
                mock.Mock<IDatabaseAccess>()
                    .Setup(x => x.SelectQueryList(_summary, sql))
                    .Returns(GetSampleData());
                var cls = mock.Create<SummaryProcessor>();
                var expected = GetSampleData();
                var actual = cls.LoadDataListByPerson(_summary);
                Assert.True(actual != null);
                Assert.Equal(expected.Count, actual.Count);
            }
        }

        private List<SummaryModel> GetSampleData()
        {
            List<SummaryModel> output = new()
            {
                new SummaryModel()
                {
                    ID = 1
                },
                new SummaryModel()
                {
                    ID = 2
                },
                new SummaryModel()
                {
                    ID = 3
                }
            };
            return output;
        }
    }
}