using System;
using System.Collections.Generic;
using System.Text;
using Moq;
using Autofac.Extras.Moq;
using ResumeVaultLib.Data.Logic;
using ResumeVaultLib.Data.Models;
using ResumeVaultLib.Data.Utilities;
using Xunit;

namespace ResumeVaultLibTest
{
    public class EducationProcessorTests
    {
        private EducationModel _education = new()
        {
            ID = 1,
            SchoolName = "University",
            Major = "Major",
            Minor = "Minor",
            StartDate = new DateTime(2005, 09, 15),
            EndDate = new DateTime(2010, 6, 15),
            GPA = 3.5M,
            Attending = false,
            PersonID = 1
        };
        [Fact]
        public void Insert_ValidCall()
        {
            using (var mock = AutoMock.GetLoose())
            {
                string sql = @"insert into Education 
                        (SchoolName, Major, Minor, StartDate, EndDate, GPA, Attending, PersonID) VALUES 
                        (@SchoolName, @Major, @Minor, @StartDate, @EndDate, @GPA, @Attending, @PersonID)";
                mock.Mock<IDatabaseAccess>()
                    .Setup(x => x.InsertQuery(_education, sql));
                var cls = mock.Create<EducationProcessor>();
                cls.Insert(_education);
                mock.Mock<IDatabaseAccess>()
                    .Verify(x => x.InsertQuery(_education, sql), Times.Exactly(1));
            }
        }

        [Fact]
        public void Update_ValidCall()
        {
            using (var mock = AutoMock.GetLoose())
            {
                string sql = @"update Education set SchoolName=@SchoolName, Major=@Major, Minor=@Minor, 
                     StartDate=@StartDate, EndDate=@EndDate, GPA=@GPA, Attending=@Attending, PersonID=@PersonID where 
                     ID=@ID";
                mock.Mock<IDatabaseAccess>()
                    .Setup(x => x.UpdateQuery(_education, sql));
                var cls = mock.Create<EducationProcessor>();
                cls.Update(_education);
                mock.Mock<IDatabaseAccess>()
                    .Verify(x => x.UpdateQuery(_education, sql), Times.Exactly(1));
            }
        }

        [Fact]
        public void Delete_ValidCall()
        {
            using (var mock = AutoMock.GetLoose())
            {
                string sql = "delete from Education where ID=@ID";
                mock.Mock<IDatabaseAccess>()
                    .Setup(x => x.DeleteQuery(_education, sql));
                var cls = mock.Create<EducationProcessor>();
                cls.Delete(_education);
                mock.Mock<IDatabaseAccess>()
                    .Verify(x => x.DeleteQuery(_education, sql), Times.Exactly(1));
            }
        }
        [Fact]
        public void LoadDataSingle_GetPersonModel()
        {
            using (var mock = AutoMock.GetLoose())
            {
                string sql = "select * from Education where ID=@ID";
                mock.Mock<IDatabaseAccess>()
                    .Setup(x => x.SelectQuerySingle(_education, sql))
                    .Returns(_education);
                var cls = mock.Create<EducationProcessor>();
                var expected = _education;
                var actual = cls.LoadDataSingle(_education);
                Assert.True(actual != null);
                Assert.Equal(expected.ID, actual.ID);
            }
        }
        [Fact]
        public void LoadDataList_ValidCall()
        {
            string sql = "select * from Education";
            using (var mock = AutoMock.GetLoose())
            {
                mock.Mock<IDatabaseAccess>()
                    .Setup(x => x.SelectQueryList(_education, sql))
                    .Returns(GetSampleData());
                var cls = mock.Create<EducationProcessor>();
                var expected = GetSampleData();
                var actual = cls.LoadAllDataList(_education);
                Assert.True(actual != null);
                Assert.Equal(expected.Count, actual.Count);
            }
        }
        [Fact]
        public void LoadDataListByPerson_ValidCall()
        {
            string sql = "select * from Education where PersonID=@PersonID";
            using (var mock = AutoMock.GetLoose())
            {
                mock.Mock<IDatabaseAccess>()
                    .Setup(x => x.SelectQueryList(_education, sql))
                    .Returns(GetSampleData());
                var cls = mock.Create<EducationProcessor>();
                var expected = GetSampleData();
                var actual = cls.LoadDataListByPerson(_education);
                Assert.True(actual != null);
                Assert.Equal(expected.Count, actual.Count);
            }
        }
        [Fact]
        public void LoadSelectDataList_validCall()
        {
            List<int> idList = new() {1, 2, 3};
            StringBuilder builder = new StringBuilder();
            foreach (var item in idList)
            {
                if (idList.IndexOf(item) == idList.Count - 1)
                {
                    builder.Append(item);
                }
                else
                {
                    builder.Append(item).Append(", ");
                }
            }
            var ids = builder.ToString();
            string sql = $"select * from Education where ID in ({ids});";
            using (var mock = AutoMock.GetLoose())
            {
                mock.Mock<IDatabaseAccess>()
                    .Setup(x => x.SelectQueryList(_education, sql))
                    .Returns(GetSampleData());
                var cls = mock.Create<EducationProcessor>();
                var expected = GetSampleData();
                var actual = cls.LoadSelectDataList(_education, idList);
                Assert.True(actual != null);
                Assert.Equal(expected.Count, actual.Count);
            }
        }

        private List<EducationModel> GetSampleData()
        {
            List<EducationModel> output = new()
            {
                new EducationModel()
                {
                    ID = 1,
                    PersonID = 1
                },
                new EducationModel()
                {
                    ID = 2,
                    PersonID = 1
                },
                new EducationModel()
                {
                    ID = 3,
                    PersonID = 2
                }
            };
            return output;
        }
    }
}