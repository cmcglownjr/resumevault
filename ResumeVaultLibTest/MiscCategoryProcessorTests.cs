using System.Collections.Generic;
using Moq;
using Autofac.Extras.Moq;
using ResumeVaultLib.Data.Logic;
using ResumeVaultLib.Data.Models;
using ResumeVaultLib.Data.Utilities;
using Xunit;

namespace ResumeVaultLibTest
{
    public class MiscCategoryProcessorTests
    {
        private MiscCategoryModel _miscCategory = new()
        {
            ID = 1
        };

        [Fact]
        public void Insert_ValidCall()
        {
            string sql = @"insert into Misc (PersonID, Name, Description, Date, Category) values 
                        (@PersonID, @Name, @Description, @Date, @Category)";
            using (var mock = AutoMock.GetLoose())
            {
                mock.Mock<IDatabaseAccess>()
                    .Setup(x => x.InsertQuery(_miscCategory, sql));
                var cls = mock.Create<MiscCategoryProcessor>();
                cls.Insert(_miscCategory);
                mock.Mock<IDatabaseAccess>()
                    .Verify(x => x.InsertQuery(_miscCategory, sql), Times.Exactly(1));
            }
        }

        [Fact]
        public void Update_ValidCall()
        {
            string sql = @"update Misc set PersonID=@PersonID, Name=@Name, Description=@Description, Date=@Date, 
                        Category=@Category where ID=@ID";
            using (var mock = AutoMock.GetLoose())
            {
                mock.Mock<IDatabaseAccess>()
                    .Setup(x => x.UpdateQuery(_miscCategory, sql));
                var cls = mock.Create<MiscCategoryProcessor>();
                cls.Update(_miscCategory);
                mock.Mock<IDatabaseAccess>()
                    .Verify(x => x.UpdateQuery(_miscCategory, sql), Times.Exactly(1));
            }
        }

        [Fact]
        public void Delete_ValidCall()
        {
            string sql = "delete from MiscCategory where ID=@ID";
            using (var mock = AutoMock.GetLoose())
            {
                mock.Mock<IDatabaseAccess>()
                    .Setup(x => x.DeleteQuery(_miscCategory, sql));
                var cls = mock.Create<MiscCategoryProcessor>();
                cls.Delete(_miscCategory);
                mock.Mock<IDatabaseAccess>()
                    .Verify(x => x.DeleteQuery(_miscCategory, sql), Times.Exactly(1));
            }
        }

        [Fact]
        public void LoadDataSingle_GetExperienceModel()
        {
            string sql = "select * from MiscCategory where ID=@ID";
            using (var mock = AutoMock.GetLoose())
            {
                mock.Mock<IDatabaseAccess>()
                    .Setup(x => x.SelectQuerySingle(_miscCategory, sql))
                    .Returns(_miscCategory);
                var cls = mock.Create<MiscCategoryProcessor>();
                var expected = _miscCategory;
                var actual = cls.LoadDataSingle(_miscCategory);
                Assert.True(actual != null);
                Assert.Equal(expected.ID, actual.ID);
            }
        }
        [Fact]
        public void LoadDataList_ValidCall()
        {
            string sql = "select * from MiscCategory";
            using (var mock = AutoMock.GetLoose())
            {
                mock.Mock<IDatabaseAccess>()
                    .Setup(x => x.SelectQueryList(_miscCategory, sql))
                    .Returns(GetSampleData());
                var cls = mock.Create<MiscCategoryProcessor>();
                var expected = GetSampleData();
                var actual = cls.LoadAllDataList(_miscCategory);
                Assert.True(actual != null);
                Assert.Equal(expected.Count, actual.Count);
            }
        }

        private List<MiscCategoryModel> GetSampleData()
        {
            List<MiscCategoryModel> output = new()
            {
                new MiscCategoryModel()
                {
                    ID = 1
                },
                new MiscCategoryModel()
                {
                    ID = 2
                },
                new MiscCategoryModel()
                {
                    ID = 3
                }
            };
            return output;
        }
    }
}