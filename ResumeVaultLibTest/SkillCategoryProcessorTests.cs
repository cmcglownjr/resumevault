using System.Collections.Generic;
using Moq;
using Autofac.Extras.Moq;
using ResumeVaultLib.Data.Logic;
using ResumeVaultLib.Data.Models;
using ResumeVaultLib.Data.Utilities;
using Xunit;

namespace ResumeVaultLibTest
{
    public class SkillCategoryProcessorTests
    {
        private SkillCategoryModel _skillCategory = new()
        {
            ID = 1
        };

        [Fact]
        public void Insert_ValidCall()
        {
            string sql = "insert into SkillCategory (Name) values (@Name)";
            using (var mock = AutoMock.GetLoose())
            {
                mock.Mock<IDatabaseAccess>()
                    .Setup(x => x.InsertQuery(_skillCategory, sql));
                var cls = mock.Create<SkillCategoryProcessor>();
                cls.Insert(_skillCategory);
                mock.Mock<IDatabaseAccess>()
                    .Verify(x => x.InsertQuery(_skillCategory, sql), Times.Exactly(1));
            }
        }

        [Fact]
        public void Update_ValidCall()
        {
            string sql = "update SkillCategory set Name=@Name where ID=@ID";
            using (var mock = AutoMock.GetLoose())
            {
                mock.Mock<IDatabaseAccess>()
                    .Setup(x => x.UpdateQuery(_skillCategory, sql));
                var cls = mock.Create<SkillCategoryProcessor>();
                cls.Update(_skillCategory);
                mock.Mock<IDatabaseAccess>()
                    .Verify(x => x.UpdateQuery(_skillCategory, sql), Times.Exactly(1));
            }
        }

        [Fact]
        public void Delete_ValidCall()
        {
            string sql = "delete from SkillCategory where ID=@ID";
            using (var mock = AutoMock.GetLoose())
            {
                mock.Mock<IDatabaseAccess>()
                    .Setup(x => x.DeleteQuery(_skillCategory, sql));
                var cls = mock.Create<SkillCategoryProcessor>();
                cls.Delete(_skillCategory);
                mock.Mock<IDatabaseAccess>()
                    .Verify(x => x.DeleteQuery(_skillCategory, sql), Times.Exactly(1));
            }
        }

        [Fact]
        public void LoadDataSingle_GetExperienceModel()
        {
            string sql = "select * from SkillCategory where ID=@ID";
            using (var mock = AutoMock.GetLoose())
            {
                mock.Mock<IDatabaseAccess>()
                    .Setup(x => x.SelectQuerySingle(_skillCategory, sql))
                    .Returns(_skillCategory);
                var cls = mock.Create<SkillCategoryProcessor>();
                var expected = _skillCategory;
                var actual = cls.LoadDataSingle(_skillCategory);
                Assert.True(actual != null);
                Assert.Equal(expected.ID, actual.ID);
            }
        }
        [Fact]
        public void LoadDataList_ValidCall()
        {
            string sql = "select * from SkillCategory";
            using (var mock = AutoMock.GetLoose())
            {
                mock.Mock<IDatabaseAccess>()
                    .Setup(x => x.SelectQueryList(_skillCategory, sql))
                    .Returns(GetSampleData());
                var cls = mock.Create<SkillCategoryProcessor>();
                var expected = GetSampleData();
                var actual = cls.LoadAllDataList(_skillCategory);
                Assert.True(actual != null);
                Assert.Equal(expected.Count, actual.Count);
            }
        }

        private List<SkillCategoryModel> GetSampleData()
        {
            List<SkillCategoryModel> output = new()
            {
                new SkillCategoryModel()
                {
                    ID = 1
                },
                new SkillCategoryModel()
                {
                    ID = 2
                },
                new SkillCategoryModel()
                {
                    ID = 3
                }
            };
            return output;
        }
    }
}