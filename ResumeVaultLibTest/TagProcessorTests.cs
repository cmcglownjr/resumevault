using System.Collections.Generic;
using Moq;
using Autofac.Extras.Moq;
using ResumeVaultLib.Data.Logic;
using ResumeVaultLib.Data.Models;
using ResumeVaultLib.Data.Utilities;
using Xunit;

namespace ResumeVaultLibTest
{
    public class TagProcessorTests
    {
        private TagModel _tag = new()
        {
            ID = 1
        };

        [Fact]
        public void Insert_ValidCall()
        {
            string sql = "insert into Tag (Name) values (@Name)";
            using (var mock = AutoMock.GetLoose())
            {
                mock.Mock<IDatabaseAccess>()
                    .Setup(x => x.InsertQuery(_tag, sql));
                var cls = mock.Create<TagProcessor>();
                cls.Insert(_tag);
                mock.Mock<IDatabaseAccess>()
                    .Verify(x => x.InsertQuery(_tag, sql), Times.Exactly(1));
            }
        }

        [Fact]
        public void Update_ValidCall()
        {
            string sql = "update Tag set Name=@Name where ID=@ID";
            using (var mock = AutoMock.GetLoose())
            {
                mock.Mock<IDatabaseAccess>()
                    .Setup(x => x.UpdateQuery(_tag, sql));
                var cls = mock.Create<TagProcessor>();
                cls.Update(_tag);
                mock.Mock<IDatabaseAccess>()
                    .Verify(x => x.UpdateQuery(_tag, sql), Times.Exactly(1));
            }
        }

        [Fact]
        public void Delete_ValidCall()
        {
            string sql = "delete from Tag where ID=@ID";
            using (var mock = AutoMock.GetLoose())
            {
                mock.Mock<IDatabaseAccess>()
                    .Setup(x => x.DeleteQuery(_tag, sql));
                var cls = mock.Create<TagProcessor>();
                cls.Delete(_tag);
                mock.Mock<IDatabaseAccess>()
                    .Verify(x => x.DeleteQuery(_tag, sql), Times.Exactly(1));
            }
        }

        [Fact]
        public void LoadDataSingle_GetExperienceModel()
        {
            string sql = "select * from Tag where ID=@ID";
            using (var mock = AutoMock.GetLoose())
            {
                mock.Mock<IDatabaseAccess>()
                    .Setup(x => x.SelectQuerySingle(_tag, sql))
                    .Returns(_tag);
                var cls = mock.Create<TagProcessor>();
                var expected = _tag;
                var actual = cls.LoadDataSingle(_tag);
                Assert.True(actual != null);
                Assert.Equal(expected.ID, actual.ID);
            }
        }
        [Fact]
        public void LoadDataList_ValidCall()
        {
            string sql = "select * from Tag";
            using (var mock = AutoMock.GetLoose())
            {
                mock.Mock<IDatabaseAccess>()
                    .Setup(x => x.SelectQueryList(_tag, sql))
                    .Returns(GetSampleData());
                var cls = mock.Create<TagProcessor>();
                var expected = GetSampleData();
                var actual = cls.LoadAllDataList(_tag);
                Assert.True(actual != null);
                Assert.Equal(expected.Count, actual.Count);
            }
        }

        private List<TagModel> GetSampleData()
        {
            List<TagModel> output = new()
            {
                new TagModel()
                {
                    ID = 1
                },
                new TagModel()
                {
                    ID = 2
                },
                new TagModel()
                {
                    ID = 3
                }
            };
            return output;
        }
    }
}