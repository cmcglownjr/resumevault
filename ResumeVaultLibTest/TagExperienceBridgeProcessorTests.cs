using System.Collections.Generic;
using Moq;
using Autofac.Extras.Moq;
using ResumeVaultLib.Data.Logic;
using ResumeVaultLib.Data.Models;
using ResumeVaultLib.Data.Utilities;
using Xunit;

namespace ResumeVaultLibTest
{
    public class TagExperienceBridgeProcessorTests
    {
        private TagExperienceBridgeModel _tagExp = new()
        {
            ID = 1
        };

        [Fact]
        public void Insert_ValidCall()
        {
            string sql = "insert into TagExperienceBridge (ExperienceID, TagID) values (@ExperienceID, @TagID)";
            using (var mock = AutoMock.GetLoose())
            {
                mock.Mock<IDatabaseAccess>()
                    .Setup(x => x.InsertQuery(_tagExp, sql));
                var cls = mock.Create<TagExperienceBridgeProcessor>();
                cls.Insert(_tagExp);
                mock.Mock<IDatabaseAccess>()
                    .Verify(x => x.InsertQuery(_tagExp, sql), Times.Exactly(1));
            }
        }

        [Fact]
        public void Update_ValidCall()
        {
            string sql = "update TagExperienceBridge set ExperienceID=@ExperienceID, TagID=@TagID where ID=@ID";
            using (var mock = AutoMock.GetLoose())
            {
                mock.Mock<IDatabaseAccess>()
                    .Setup(x => x.UpdateQuery(_tagExp, sql));
                var cls = mock.Create<TagExperienceBridgeProcessor>();
                cls.Update(_tagExp);
                mock.Mock<IDatabaseAccess>()
                    .Verify(x => x.UpdateQuery(_tagExp, sql), Times.Exactly(1));
            }
        }

        [Fact]
        public void Delete_ValidCall()
        {
            string sql = "delete from TagExperienceBridge where ID=@ID";
            using (var mock = AutoMock.GetLoose())
            {
                mock.Mock<IDatabaseAccess>()
                    .Setup(x => x.DeleteQuery(_tagExp, sql));
                var cls = mock.Create<TagExperienceBridgeProcessor>();
                cls.Delete(_tagExp);
                mock.Mock<IDatabaseAccess>()
                    .Verify(x => x.DeleteQuery(_tagExp, sql), Times.Exactly(1));
            }
        }

        [Fact]
        public void LoadDataSingle_GetExperienceModel()
        {
            string sql = "select * from TagExperienceBridge where ID=@ID";
            using (var mock = AutoMock.GetLoose())
            {
                mock.Mock<IDatabaseAccess>()
                    .Setup(x => x.SelectQuerySingle(_tagExp, sql))
                    .Returns(_tagExp);
                var cls = mock.Create<TagExperienceBridgeProcessor>();
                var expected = _tagExp;
                var actual = cls.LoadDataSingle(_tagExp);
                Assert.True(actual != null);
                Assert.Equal(expected.ID, actual.ID);
            }
        }
        [Fact]
        public void LoadDataList_ValidCall()
        {
            string sql = "select * from TagExperienceBridge";
            using (var mock = AutoMock.GetLoose())
            {
                mock.Mock<IDatabaseAccess>()
                    .Setup(x => x.SelectQueryList(_tagExp, sql))
                    .Returns(GetSampleData());
                var cls = mock.Create<TagExperienceBridgeProcessor>();
                var expected = GetSampleData();
                var actual = cls.LoadAllDataList(_tagExp);
                Assert.True(actual != null);
                Assert.Equal(expected.Count, actual.Count);
            }
        }

        private List<TagExperienceBridgeModel> GetSampleData()
        {
            List<TagExperienceBridgeModel> output = new()
            {
                new TagExperienceBridgeModel()
                {
                    ID = 1
                },
                new TagExperienceBridgeModel()
                {
                    ID = 2
                },
                new TagExperienceBridgeModel()
                {
                    ID = 3
                }
            };
            return output;
        }
    }
}