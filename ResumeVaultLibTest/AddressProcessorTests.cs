using System;
using System.Collections.Generic;
using Moq;
using Autofac.Extras.Moq;
using ResumeVaultLib.Data.Logic;
using ResumeVaultLib.Data.Models;
using ResumeVaultLib.Data.Utilities;
using Xunit;

namespace ResumeVaultLibTest
{
    public class AddressProcessorTests
    {
        private AddressModel _address = new()
        {
            ID = 1,
            PersonID = 1,
            Address1 = "123 Main St.",
            Address2 = "Apt. 2",
            City = "NowheresVille",
            StateProvence = 30,
            Zip = "132587"
        };

        [Fact]
        public void Insert_ValidCall()
        {
            string sql =  @"insert into Address (PersonID, Address1, Address2, City, StateProvence, Zip, Name) 
                            VALUES (@PersonID, @Address1, @Address2, @City, @StateProvence, @Zip, @Name)";
            using (var mock = AutoMock.GetLoose())
            {
                mock.Mock<IDatabaseAccess>()
                    .Setup(x => x.InsertQuery(_address, sql));
                var cls = mock.Create<AddressProcessor>();
                cls.Insert(_address);
                mock.Mock<IDatabaseAccess>()
                    .Verify(x => x.InsertQuery(_address, sql), Times.Exactly(1));
            }
        }

        [Fact]
        public void Update_ValidCall()
        {
            string sql = @"update Address set PersonID=@PersonID, Address1=@Address1, Address2=@Address2, 
                            City=@City, StateProvence=@StateProvence, Zip=@Zip, Name=@Name where ID=@ID";
            using (var mock = AutoMock.GetLoose())
            {
                mock.Mock<IDatabaseAccess>()
                    .Setup(x => x.UpdateQuery(_address, sql));
                var cls = mock.Create<AddressProcessor>();
                cls.Update(_address);
                mock.Mock<IDatabaseAccess>()
                    .Verify(x => x.UpdateQuery(_address, sql), Times.Exactly(1));
            }
        }

        [Fact]
        public void Delete_ValidCall()
        {
            string sql = "delete from Address where ID=@ID";
            using (var mock = AutoMock.GetLoose())
            {
                mock.Mock<IDatabaseAccess>()
                    .Setup(x => x.DeleteQuery(_address, sql));
                var cls = mock.Create<AddressProcessor>();
                cls.Delete(_address);
                mock.Mock<IDatabaseAccess>()
                    .Verify(x => x.DeleteQuery(_address, sql), Times.Exactly(1));
            }
        }

        [Fact]
        public void LoadDataSingle_GetExperienceModel()
        {
            string sql = "select * from Address where ID=@ID";
            using (var mock = AutoMock.GetLoose())
            {
                mock.Mock<IDatabaseAccess>()
                    .Setup(x => x.SelectQuerySingle(_address, sql))
                    .Returns(_address);
                var cls = mock.Create<AddressProcessor>();
                var expected = _address;
                var actual = cls.LoadDataSingle(_address);
                Assert.True(actual != null);
                Assert.Equal(expected.ID, actual.ID);
            }
        }
        [Fact]
        public void LoadDataList_ValidCall()
        {
            string sql = "select * from Address";
            using (var mock = AutoMock.GetLoose())
            {
                mock.Mock<IDatabaseAccess>()
                    .Setup(x => x.SelectQueryList(_address, sql))
                    .Returns(GetSampleData());
                var cls = mock.Create<AddressProcessor>();
                var expected = GetSampleData();
                var actual = cls.LoadAllDataList(_address);
                Assert.True(actual != null);
                Assert.Equal(expected.Count, actual.Count);
            }
        }
        
        [Fact]
        public void LoadDataListByPerson_ValidCall()
        {
            string sql = @"select * from Address where PersonID=@PersonID";
            using (var mock = AutoMock.GetLoose())
            {
                mock.Mock<IDatabaseAccess>()
                    .Setup(x => x.SelectQueryList(_address, sql))
                    .Returns(GetSampleData());
                var cls = mock.Create<AddressProcessor>();
                var expected = GetSampleData();
                var actual = cls.LoadDataListByPerson(_address);
                Assert.True(actual != null);
                Assert.Equal(expected.Count, actual.Count);
            }
        }

        private List<AddressModel> GetSampleData()
        {
            List<AddressModel> output = new()
            {
                new AddressModel()
                {
                    ID = 1
                },
                new AddressModel()
                {
                    ID = 2
                },
                new AddressModel()
                {
                    ID = 3
                }
            };
            return output;
        }
    }
}